<?php

class SearchController extends ModuleWorkUpController 
{
	public function mSearchProfil() {
		if (Input::has('searchString')) {
			$searchString = Input::get('searchString');
		} else $searchString = ' steuber ';
			$searchString = trim($searchString);
			$users = User::where('first_name','LIKE',  "%$searchString%")
						->orWhere('last_name','LIKE', "%$searchString%")
						->orWhere('first_name','LIKE', "%$searchString")
						->orWhere('last_name','LIKE', "%$searchString")
						->orWhere('first_name','LIKE', "$searchString%")
						->orWhere('last_name','LIKE', "$searchString%")
						->limit(5)->get();
			$output = array();
			if (!$users->isEmpty()) {
				$i = 0;
				foreach($users as $user) {
					$output[$i]['id'] = $user->id;
					$output[$i]['first_name'] = $user->first_name;
					$output[$i]['last_name'] = $user->last_name;
					$output[$i]['city'] = $user->city;
					$output[$i]['adress'] = $user->address;
					$output[$i]['mail'] = $user->mail;
					$output[$i]['phone'] = $user->phone;
					$output[$i]['postal_code'] = $user->postal_code;
					$output[$i]['isMale'] = $user->isMale;
					$output[$i]['birthday'] = $user->birthday;
					//Get Profil and cover picure
					$userImageCouv = UserImage::where('user_id',$user->id)->where('active',true)->where('type','couverture')->first();
					if (!empty($userImageCouv)) {
						$imageCouv = WorkUpImage::where('id',$userImageCouv->image_id)->first();
						$output[$i]['couverture'] = 'public/uploads/user/' . $user->id . '/cover-picture/' . $imageCouv->file_name . '.' . $imageCouv->file_ext;
					} else {
						$output[$i]['couverture'] = '';
					}
					$userImageProfil = UserImage::where('user_id',$user->id)->where('active',true)->where('type','profil')->first();
					if (!empty($userImageProfil)) {
						$image = WorkUpImage::where('id',$userImageProfil->image_id)->first();
						$output[$i]['profil'] = 'public/uploads/user/' . $user->id . '/profil-picture/' . $image->file_name . '.' . $image->file_ext;
					} else {
						$output[$i]['profil'] = '';
					}
					if (Client::where('user_id',$user->id)->first()) {
						$output[$i]['response'] = 'CU';
					} else {
						$ae = AutoContractor::where('user_id',$user->id)->first();
						if (!empty($ae)) {
							$output[$i]['response'] = 'AE';
							$output[$i]['biography'] = $ae->biography;
							$output[$i]['siren'] = $ae->siren;
							$output[$i]['job_category'] = $ae->getJobCategory();
						} else {
							$output[$i]['response'] = 'Inconnu';
						}
					}
					$i++;	
				}
			} else {
				$output['response'] = false;
			}
			$data = Response::json($output)->header('Content-Type', 'application/json');
			echo $data->getContent();
	}

	 public function createSearch(){
	 	if (Input::has('searchString')) {
			$searchString = Input::get('searchString');
			$searchString = trim($searchString);
			$users = User::where('first_name','LIKE',  "%$searchString%")
						->orWhere('last_name','LIKE', "%$searchString%")
						->orWhere('first_name','LIKE', "%$searchString")
						->orWhere('last_name','LIKE', "%$searchString")
						->orWhere('first_name','LIKE', "$searchString%")
						->orWhere('last_name','LIKE', "$searchString%")
						->limit(5)->get();
			$this->data['result'] = $users;				
          	return View::make('theme::public.search-result', $this->data);
        }
    }

}