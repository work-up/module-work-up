<?php

class ProfilController extends ModuleWorkUpController 
{

	public $validExtention = array('png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG');

	public function create() {
		return View::make('theme::public.user.profil-page',$this->data);
	}
	public function createEdit() {
		return View::make('theme::public.user.profil-page-edit',$this->data);
	}

	public function getPage($slug)
    {
        $user = User::where('slug',$slug)->first();
        if(!empty($user)) {
        	$userGallery = UserGallery::where('user_id',$user->id)->first();
        	if (!empty($userGallery)) {
        		$gallery = Gallery::where('id',$userGallery->gallery_id)->first();
	   			$this->data['gallery'] = $gallery;
	   		}
        	if (Auth::check()){
        		$auth = Auth::user();
        		if (!empty($auth)) {
	        		$authUser = DB::table('auth_user')->where('auth_id', $auth->id)->first(array('user_id'));
	        		if (!empty($authUser)) {
	        			$current_user = User::where('id',$authUser->user_id)->first();
	        			if ($user->id == $current_user->id) {
	        				return View::make('theme::public.user.profil-page-edit',$this->data);
	        			}
	        		}
	        	}
        	}
        	$this->data['userPublic'] = $user;
        	return View::make('theme::public.user.profil-page',$this->data);
        }
        return View::make('theme::public.error.404');    
    }


    public function updateInfo() {
    	$user = User::where('id', Input::get('user_id'))->first();
		if (Input::has('phone')) {
			$user->phone = Input::get('phone');
		}
		if (Input::has('adress')) {
			$user->address = Input::get('adress');
		}
		if (Input::has('postal_code')) {
			$user->postal_code = Input::get('postal_code');
		}
		if (Input::has('city')) {
			$user->city = Input::get('city');
		}
		if (Input::has('compagny')) {
			$ae = AutoContractor::where('user_id',Input::get('user_id'))->first();
			if(!empty($ae)) {
				$ae->compagny = Input::get('compagny');
				if (!$ae->save()) {
					if (Request::ajax()) {
						return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
					} else {
						return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
					}
				}
			}
		}
		if (Input::has('categorie_metier')) {
			$ae = AutoContractor::where('user_id',Input::get('user_id'))->first();
			if(!empty($ae)) {
				$id_job= Input::get('categorie_metier');
				if (empty($id_job) && $id_job > 0) {
					$ae->attachJobCategory($id_job);
				}	
			}
		}
		if (Input::has('description')) {
			$ae = AutoContractor::where('user_id',Input::get('user_id'))->first();
			if(!empty($ae)) {
				$ae->biography = Input::get('description');
				if (!$ae->save()) {
					if (Request::ajax()) {
						return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
					} else {
						return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
					}
				}
			}
		}
		if (!$user->save()) {
			if (Request::ajax()) {
				return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
			} else {
				return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
			}
		}
		$userReturn = User::where('id',Input::get('user_id'))->first();
		if (Request::ajax()) {
			return Response::json(array('statut' => 'success', 'message' => 'Votre profil a bien été mis à jour:)'));
		} else {
			return Redirect::action('ProfilController@getPage', $userReturn->slug);
		} 

    }

    public function postContact() {
    	//INPUT
    	if (Input::has('title')) {
    		$objetMessage = Input::get('title');
    	} else {
    		if (Request::ajax()) {
				return Response::json(array('statut' => 'danger', 'message' => 'Vous devez renseigner un objet !'));
			} else {
				return Redirect::back()->withInput()->with('error', 'Vous devez renseigner un objet !');
			}
    	}
    	if (Input::has('message')) {
    		$corpsMessage = Input::get('message');
    	} else {
    		if (Request::ajax()) {
				return Response::json(array('statut' => 'danger', 'message' => 'Vous devez renseigner un message !'));
			} else {
				return Redirect::back()->withInput()->with('error', 'Vous devez renseigner un message !');
			}
    	}
    	$destinataire = User::find(Input::get('userPublic'));
    	$expediteur = User::find(Input::get('user'));
    	// SEND MAIL
    	$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPAuth   = true;
        $mail->Host       = "smtp.gmail.com";
        $mail->Port       = 587;
        $mail->Username   = "Workup.Project@gmail.com";
        $mail->Password   = "kLd2KMMx";
        $mail->smtpConnect( array(
        	"ssl" => array(
            "verify_peer" => false,
            "verify_peer_name" => false,
            "allow_self_signed" => true
    		)
		));
        $mail->SMTPSecure = 'tls';                                    
		$mail->SetFrom('Workup.Project@gmail.com','Work-Up');
		$mail->FromName = $expediteur->mail ;
		$mail->addAddress($destinataire->mail);         
		$mail->isHTML(false);                                  
		$mail->Subject = $objetMessage;
		$mail->Body    = "Bonjour, " . $destinataire->last_name . ' ' . $destinataire->first_name ." vous avez reçu un message de la part de : " . $expediteur->last_name . ' ' . $expediteur->first_name . 
		" depuis l'application Work-Up :<br><br>" . $corpsMessage . '<br><br> Cliquez sur lien pour consulter son profil : http://work-up.fr/profil/' . $expediteur->slug . '<br><br>';
		$mail->AltBody = 'Work-Up vous remercie pour votre intérêt ! :)';
		if(!$mail->send()) {
			if (Request::ajax()) {
				return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'envoi du mail !'));
			} else {
				return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'envoi du mail !');
			}
		} else {
			if (Request::ajax()) {
				return Response::json(array('statut' => 'success', 'message' => 'Un mail a été envoyé à l\'utilisateur!'));
      		} else {
        		return Redirect::back()->withInput()->with('sucess', 'Un mail a été envoyé à l\'utilisateur!');
			}
		}

    }


	public function mUpdateInfo($id, $birthday, $adresse, $codePostal, $ville, $telephone) {

		$user = User::where('id',$id)->first();
		$user->address = urldecode($adresse);
		$user->postal_code = urldecode($codePostal);
		$user->city = urldecode($ville);
		$user->phone = urldecode($telephone);
		$user->birthday = urldecode($birthday);
		if (!$user->save()) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json($output)->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$output = array();
		$output['response'] = 'true';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
	}

	public function mUpdateInfoAe($id, $birthday, $adresse, $codePostal, $ville, $telephone, $password, $experience, $jobCategory ) {

		$user = User::where('id',$id)->first();
		$user->address = urldecode($adresse);
		$user->postal_code = urldecode($codePostal);
		$user->city = urldecode($ville);
		$user->phone = urldecode($telephone);
		$user->birthday = urldecode($birthday);
		if (!$user->save()) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json($output)->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		if(!empty($password)){
			$authUser = DB::table('auth_user')->where('user_id', $id)->first();
			$auth = AuthUser::where('id',$authUser->auth_id)->first();
			$auth->password = MCrypt::decrypt(urldecode($password));
			if (!$auth->save()) {
				$output = array();
				$output['response'] = 'false';
				$data = Response::json($output)->header('Content-Type', 'application/json');
				echo $data->getContent();
				exit();
			}
		} 
		$ae = AutoContractor::where('user_id',$user->id)->first();
		$ae->biography = urldecode($experience);
		if (!$ae->save()) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		//Marche que pour une seul category
		$aejb = AutoContractorJobCategory::where('auto_contractor_id',$ae->id)->first();
		$aejb->job_category_id = $jobCategory;
		if (!$aejb->save()) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$output = array();
		$output['response'] = 'true';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
	}

	public function mGetGallery($id,$nomAlbum) {
		$userGallery = UserGallery::where('user_id',$id)->where('name',urldecode($nomAlbum))->first();
		if(!empty($userGallery)){
			$imgGalleries = GalleryImage::where('gallery_id',$userGallery->gallery_id)->get();
			$output = array();
			$i = 0;
			foreach ($imgGalleries as $imgGallery) {
				$image = WorkUpImage::where('id',$imgGallery->image_id)->first();
				$output[$i] =  asset('uploads/user/' . $id . '/galleries/' . urldecode($nomAlbum) . '/' . $image->file_name . '.' . $image->file_ext);
			  	$i++;
			 }
			 $data = Response::json($output)->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}  
		$output = array();
		$output['response'] = 'false';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
	}

	public function mCreateAlbum($id,$nomAlbum) {
		$album = urldecode($nomAlbum);
		if(!empty($id) || !empty($album)) {
			$exist = UserGallery::where('name', $album)->where('user_id',$id)->first();
			if (!empty($exist)){
				$output = array();
				$output['response'] = 'exist';
				$data = Response::json([$output])->header('Content-Type', 'application/json');
				echo $data->getContent();
				exit();	
			}
			$ug = UserGallery::add($id,$album);
			if(!empty($ug)) {
				$output = array();
				$output['response'] = 'true';
				$data = Response::json([$output])->header('Content-Type', 'application/json');
				echo $data->getContent();
				exit();	
			}
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
		}
	}

	public function mDeleteAlbum($id,$nomAlbum) {
		$album = urldecode($nomAlbum);
		if(!empty($id) || !empty($album)) {
			$ug = UserGallery::supp($id,$album);
			if($ug) {
				$output = array();
				$output['response'] = 'true';
				$data = Response::json([$output])->header('Content-Type', 'application/json');
				echo $data->getContent();
				exit();	
			}
		}
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
	}

	public function mGetAlbum($id) {
		$ugs = UserGallery::where('user_id',$id)->get();
		if(empty($ugs)) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();
		}
		$output = array();
		foreach($ugs as $ug) {
			$object = new stdClass();
			$object->name = $ug->name;
			$gallery = Gallery::where('id',$ug->gallery_id)->first();
			if(!empty($gallery->cover_image_id)) {
				$image = WorkUpImage::where('id',$gallery->cover_image_id)->first();
				$object->first_image =  asset('uploads/user/' . $id . '/galleries/' . $ug->name . '/' . $image->file_name . '.' . $image->file_ext);
			}
			$data[] = $object;
		}
		$dat = Response::json($data)->header('Content-Type', 'application/json');
		echo $dat->getContent();
	}

	public function mGetPicture($id, $type){
		$userImage = UserImage::where('user_id',$id)->where('active',true)->where('type',$type)->first();
		if (!empty($userImage)) {
			$image = WorkUpImage::where('id',$userImage->image_id)->first();
			$output = array();
			if ($type == 'couverture' ) {
				$output['response'] =  asset('uploads/user/' . $id . '/cover-picture/' . $image->file_name . '.' . $image->file_ext);
			} else {
				$output['response'] =  asset('uploads/user/' . $id . '/profil-picture/' . $image->file_name . '.' . $image->file_ext);
			}
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$output = array();
		$output['response'] = 'false';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();	
	}

	public function mAddPicture() {

		//récupération des parametres 
		if (Input::has('id')) {
			$id = Input::get('id');

			if (Input::has('nomAlbum')) {
				$nomAlbum = Input::get('nomAlbum');
			}
			if (Input::has('fileExt')) {
				$fileExt = Input::get('fileExt');
			}
		} else {
			$output = array();
			$output['response'] = 'false';
			$output['error'] = 'problem with post parameters';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}

		//$data = urldecode($data);
		
		if (Input::has('data_image')) {
			$data = Input::get('data_image');
		} else {
			$data = 'data:image/png;base64,AAAFBfj42Pj4';
		}
	
		$fileName = Helpers::makeFileName('rand');

		$image = WorkUpImage::add($fileName,urldecode($fileExt));
		$userGallery = UserGallery::where('user_id',$id)->where('name',urldecode($nomAlbum))->first();
		if (empty($userGallery)) {
			$output = array();
			$output['response'] = 'false';
			$output['error'] = 'album or user not exist';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		if (!file_exists('/var/www/dynamix/current/public/uploads/user/' . $id . '/galleries/' . $nomAlbum . '/')) {
			File::makeDirectory('/var/www/dynamix/current/public/uploads/user/' . $id . '/galleries/' . $nomAlbum . '/', 0777, true, true);
		}
		$filePath = '/var/www/dynamix/current/public/uploads/user/' . $id . '/galleries/' . $nomAlbum . '/';
		

		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
		base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
		file_put_contents($filePath . $fileName . '.' . $fileExt, $data);


		$galleryImage = new GalleryImage();
		$galleryImage->gallery_id = $userGallery->gallery_id;
		$galleryImage->image_id = $image->id;
		$galleryImageExist = GalleryImage::where('gallery_id',$userGallery->gallery_id)->orderBy('order','desc')->first();
		if (empty($galleryImageExist)) {
			$gallery = Gallery::where('id',$userGallery->gallery_id)->first();
			$gallery->cover_image_id = $image->id;
			$gallery->save();
			$galleryImage->order = 1;
		} else {
			$galleryImage->order = $galleryImageExist->order + 1;
		}
		if (!$galleryImage->save()) {
			$output = array();
			$output['response'] = 'false';
			$output['error'] = 'error save in BDD';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$output = array();
		$output['response'] = 'public/uploads/user/' . $id . '/galleries/' . $nomAlbum . '/' . $image->file_name . '.' . $image->file_ext;
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();

	}

	public function mDeletePicture($id,$nomAlbum,$fileName) {
		$image = WorkUpImage::where('file_name',urldecode($fileName))->first();
		if(empty($image)) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$ug = GalleryImage::where('image_id', $image->id)->first();
		if (empty($ug)) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		if(!$ug->delete()){
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		if(!$image->delete()){
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$output = array();
		$output['response'] = 'true';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();	
	}

	public function mCreateAsk() {
		if (Input::has('redacteur')) {
			$redacteur = Input::get('redacteur');
			if (Input::has('destination')) {
				$destination = Input::get('destination');
				if (Input::has('titre')) {
					$titre = Input::get('titre');
					if (Input::has('description')) {
						$description = Input::get('description');
						if (Input::has('budget')) {
							$budget = Input::get('budget');
							if (Input::has('dateDebut')) {
								$dateDebut = Input::get('dateDebut');
								if (Input::has('lieu')) {
									$lieu = Input::get('lieu');
								}
								$response = BenefitRequest::add($titre,$description,$dateDebut,$budget,$lieu,$redacteur,$destination);
	   							if ($response){
	   								$output = array();
									$output['response'] = 'true';
									$data = Response::json([$output])->header('Content-Type', 'application/json');
									echo $data->getContent();
									exit();
								}
							}
						}
					}
				}
			}
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();	
	}

	public function mContactMail() {
		if (Input::has('redacteur')) {
			$redacteur = Input::get('redacteur');
			if (Input::has('destination')) {
				$destination = Input::get('destination');
				if (Input::has('objetmessage')) {
					$objetMessage = Input::get('objetmessage');
					if (Input::has('corpsmessage')) {
						$corpsMessage = Input::get('corpsmessage');
					}
					$userRedacteur = User::where('id',$redacteur)->first();
					$userDestination = User::where('id',$destination)->first();
					$mail = new PHPMailer;
					$mail->isSMTP();
					$mail->SMTPAuth   = true;
			        $mail->Host       = "smtp.gmail.com";
			        $mail->Port       = 587;
			        $mail->Username   = "Workup.Project@gmail.com";
			        $mail->Password   = "kLd2KMMx";
			        $mail->smtpConnect( array(
			        	"ssl" => array(
			            "verify_peer" => false,
			            "verify_peer_name" => false,
			            "allow_self_signed" => true
		        		)
		    		));
			        $mail->SMTPSecure = 'tls';                                    
					$mail->SetFrom('Workup.Project@gmail.com','Work-Up');
					$mail->FromName = $userRedacteur->mail ;
					$mail->addAddress($userDestination->mail);         
					$mail->isHTML(false);                                  
					$mail->Subject = $objetMessage;
					$mail->Body    = "Bonjour, " . $userDestination->last_name . ' ' . $userDestination->first_name ." vous avez reçu un message de la part de : " . $userRedacteur->last_name . ' ' . $userRedacteur->first_name . " depuis l'application Work-Up :<br><br>" . $corpsMessage;
					$mail->AltBody = 'Work-Up vous remercie pour votre intérêt ! :)';
					$output = array();
					if(!$mail->send()) {
						$output['response'] = 'false';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();
					} else {
						$output['response'] = 'true';
						$data = Response::json($output)->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();
					}
				}
			}
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();
	}

	public function mResponseDemande() {
		if (Input::has('id_demande')) {
			$id_demande = Input::get('id_demande');
			if (Input::has('response')) {
				$response = Input::get('response');
				$demande = BenefitRequest::where('id',$id_demande)->first();
				if ($response == 'oui') {
					$demande->state = 'accepte';
					if ($demande->save()) {
						$output = array();
						$output['response'] = 'true';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();
					}
				} else if($response == 'non') {
					$demande->state = 'refuse';
					if ($demande->save()) {
						$output = array();
						$output['response'] = 'true';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();
					}
				} else if($response == 'plus infos') {
					$demande->state = 'negociation';
					if (Input::has('message')) {
						$message = Input::get('message');
						 if (BenefitRequestDetails::addMessage($message,$id_demande)) {
						 	if ($demande->save()) {
						 		$output = array();
								$output['response'] = 'true';
								$data = Response::json([$output])->header('Content-Type', 'application/json');
								echo $data->getContent();
								exit();
						 	}
						 }
					} else {
						$output = array();
						$output['response'] = 'false';
						$output['error'] = 'message absent';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();
					}

				} else {
					$output = array();
					$output['response'] = 'false';
					$output['error'] = 'response pas egal a oui ou non';
					$data = Response::json([$output])->header('Content-Type', 'application/json');
					echo $data->getContent();
					exit();
				}
			}
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();
	}

	public function mGetAsks() {
	
		if (Input::has('destinataire')) {
			$destinataire = Input::get('destinataire');
		/* POUR TESTER}
		$destinataire = $id; */

			$ae = AutoContractor::where('user_id',$destinataire)->first();
			
			if(empty($ae)) {
				$benefitRequests = BenefitRequest::where('user_id',$destinataire)->get();
			} else {
				$benefitRequests = BenefitRequest::where('auto_contractor_id',$ae->id)->get();
			}
			$output = array();
			$i = 0;
			foreach ($benefitRequests as $benefitRequest) {
				$user = User::find($benefitRequest->user_id);
				$output[$i]['id'] = $benefitRequest->id;
				$output[$i]['title'] = $benefitRequest->title;
				$output[$i]['description'] = $benefitRequest->description;
				$output[$i]['debut_date'] = $benefitRequest->debut_date;
				$output[$i]['state'] = $benefitRequest->state;
				$output[$i]['budget'] = $benefitRequest->budget;
				$output[$i]['city'] = $benefitRequest->city;
				$output[$i]['created_at'] = $benefitRequest->created_at;
				$output[$i]['destination_name'] = $benefitRequest->getDestinationName();
				$output[$i]['redactor_name'] = $benefitRequest->getRedactorName();
				$output[$i]['profil_picture'] = $user->getPathPicture('profil');
				$output[$i]['mail'] = $benefitRequest->getRedactorMail();
				$brq = BenefitRequestDetails::where('benefit_request_id',$benefitRequest->id)->first();
				if (!empty($brq)) {
					if(!empty($brq->message)) {
						$output[$i]['message'] = $brq->message;
					} else $output[$i]['message'] = '';
					if(!empty($brq->response)) {
						$output[$i]['response'] = $brq->response;
					} else $output[$i]['response'] = '';
				} else {
					$output[$i]['message'] = '';
					$output[$i]['response'] = '';
				}
				$i++;
			 }
			$data = Response::json($output)->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit(); 
	}

	public function mGetDevis() {
		if (Input::has('destinataire')) {
			$destinataire = Input::get('destinataire');
			$ae = AutoContractor::where('user_id',$destinataire)->first();
			$user = User::find($destinataire);
			if(!empty($ae)) {
				$quotationss = $ae->getAllQuotation();
			} else {
				$quotationss = $user->getAllQuotation();
			}
			$output = array();
			$i = 0;
				if (!empty($quotationss)) {		
					foreach ($quotationss as $quotations) {
		
						foreach ($quotations as $quotation) {

							$br = $quotation->getBenefitInfos();
							$output[$i]['benefit_id'] = $br->id;
							$output[$i]['title'] = $br->title;
							$output[$i]['description'] = $br->description;
							$output[$i]['debut_date'] = $br->debut_date;
							$output[$i]['state'] = $br->state;
							$output[$i]['budget'] = $br->budget;
							$output[$i]['city'] = $br->city;
							$output[$i]['created_at'] = $br->created_at;
							$output[$i]['destination_name'] = $br->getDestinationName();
							$output[$i]['redactor_name'] = $br->getRedactorName();
							$output[$i]['redactor_adress'] = $br->getRedactorAdress();
							$output[$i]['profil_picture'] = $user->getPathPicture('profil');
							$output[$i]['data_html'] = $quotation->data_html;
						}
					$i++;
					}
				} else {
					$output['response'] = 'false';
					$output['details'] = 'pas de devis !';
					$data = Response::json([$output])->header('Content-Type', 'application/json');
					echo $data->getContent();
					exit();
				}
			$data = Response::json($output)->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();	 
	}
	
	public function mresponseAskDetails() {
		if (Input::has('id')) {
			$id = Input::get('id');
			if (Input::has('response')) {
				$response = Input::get('response');
				$brd = BenefitRequestDetails::where('benefit_request_id',$id)->first();
				$br = BenefitRequest::find($id);
				if(!empty($brd)) {
					$brd->response = $response;
					$br->state = 'en cours';
					if (!$br->save()) {
						$output = array();
						$output['response'] = 'false';
						$output['error'] = 'Probleme enregistrement demande';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();	 
					}
					if ($brd->save()) {
						$output = array();
						$output['response'] = 'true';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();	 	
					} else {
						$output = array();
						$output['response'] = 'false';
						$output['error'] = 'wrong benefit_request_id';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();	 
						}
					}
				}
			}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();
		}	 

	public function mcreateDevis() {
		if (Input::has('redacteur')) {
			$redacteur = Input::get('redacteur');
			if (Input::has('destinataire')) {
				$destinataire = Input::get('destinataire');
				$br = BenefitRequest::where('auto_contractor_id',$destinataire)->where('user_id',$redacteur)->first();
				if(!empty($br)) {
					$devis = new Quotation();
					$devis->save();
					$brq = new BenefitRequestQuotation();
					$brq->benefit_request_id = $benefitRequest->id;
					$brq->quotation_id = $devis->id;
					$brq->state = 'a faire';
					if ($brq->save()){
						$output = array();
						$output['response'] = 'true';
						$output['quotation_id'] = $devis->id;
						$data = Response::json($output)->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();	
					}
				}
				$output = array();
				$output['response'] = 'false';
				$output['error'] = 'pas de demande existante';
				$data = Response::json($output)->header('Content-Type', 'application/json');
				echo $data->getContent();
				exit();
			}
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();
			
	}

	public function mupdateDevis() {
		if (Input::has('id_demande') && Input::has('response') ) {
			$id_demande = Input::get('id_demande');
			$response = Input::get('response');
			$br = BenefitRequest::find($id_demande);
			if(!empty($br)) {
				$brq = BenefitRequestQuotation::where('benefit_request_id',$id_demande)->first();
				if (!empty($brq)) {
					if($response == 'oui') {
						$brq->state = 'accepte';
					} else if ($response == 'non') {
						$brq->state = 'refuse';
					} else {
						$output = array();
						$output['response'] = 'false';
						$output['error'] = 'response doit avoir une valeur à oui ou non';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();
					}
					if ($brq->save()){
						$output = array();
						$output['response'] = 'true';
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();	
					}
				}
			}
			$output = array();
			$output['response'] = 'false';
			$output['error'] = 'pas de demande existante';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();
			
	}

	public function mgetBenefits($id) {
	
		/*if (Input::has('destinataire')) {
			$destinataire = Input::get('destinataire');*/
		 
		$destinataire = $id;

			$ae = AutoContractor::where('user_id',$destinataire)->first();
			$userDest = User::find($destinataire);
			
			if(empty($ae)) {
				$benefits = Benefit::where('user_id',$destinataire)->get();
			} else {
				$benefits = Benefit::where('auto_contractor_id',$ae->id)->get();
			}
			$output = array();
			$i = 0;
			foreach ($benefits as $benefit) {
				$user = User::find($benefit->user_id);
				if(!empty($user)) {
					$output[$i]['user_id'] = $user->id;
					$output[$i]['user_last_name'] = $user->last_name;
					$output[$i]['user_first_name'] = $user->first_name;
					$output[$i]['profil_picture'] = $user->getPathPicture('profil');
				}
				$output[$i]['id'] = $benefit->id;
				$output[$i]['title'] = $benefit->title;
				$output[$i]['description'] = $benefit->description;
				$output[$i]['begin_date'] = $benefit->begin_date;
				$output[$i]['end_date'] = $benefit->end_date;
				$output[$i]['state'] = $benefit->state;
				$output[$i]['auto_contractor_id'] = $benefit->auto_contractor_id;
				$output[$i]['created_at'] = $benefit->created_at;
				$output[$i]['progress'] = $benefit->progress;
				$output[$i]['userdest_last_name'] = $userDest->last_name;
				$output[$i]['userdest_first_name'] = $userDest->first_name;
				$output[$i]['profil_picture'] = $userDest->getPathPicture('profil');
				$i++;
			 }
			$data = Response::json($output)->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		/*}
		
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'parametre(s) manquant(s)';
		$data = Response::json($output)->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();*/
	}
}
