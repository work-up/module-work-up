<?php

class ModuleWorkUpController extends Controller {

	public function __construct()
    {
        if (Auth::check()) {
        	$auth = Auth::user();
    		if (!empty($auth)) {
        		$authUser = DB::table('auth_user')->where('auth_id', $auth->id)->first(array('user_id'));
        		if (!empty($authUser)) {
        			$user = User::where('id',$authUser->user_id)->first();
           	 		$this->data['user'] = $user;
           	 	}
           	 }
        }
    }

    public function getHome() {
        if (Auth::check()) {
    	   return View::make('theme::public.home', $this->data);
        }
         return View::make('theme::public.home');
    }

}