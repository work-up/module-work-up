<?php
class PdfController extends ModuleWorkUpController
{
    public function index()
    {
        $pdf = App::make('dompdf');
        $pdf->loadHTML('<!DOCTYPE html>
        <!-- saved from url=(0105)https://s3-eu-west-1.amazonaws.com/htmlpdfapi.production/free_html5_invoice_templates/example1/index.html -->
        <html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta charset="utf-8">
            <title>Work-Up Devis</title>
            <link rel="stylesheet" href="./Example 1_files/style.css" media="all">
          <style>.clearfix:after {
          content: "";
          display: table;
          clear: both;
        }

        a {
          color: #5D6975;
          text-decoration: underline;
        }

        body {
          position: relative;
          width: 100%;  
          height: 100%; 
          margin: 0 auto; 
          color: #001028;
          background: #FFFFFF; 
          font-family: Arial, sans-serif; 
          font-size: 12px; 
          font-family: Arial;
        }

        header {
          padding: 10px 0;
          margin-bottom: 30px;
        }

        #logo {
          text-align: center;
          margin-bottom: 10px;
        }

        #logo img {
          width: 90px;
        }

        h1 {
          border-top: 1px solid  #5D6975;
          border-bottom: 1px solid  #5D6975;
          color: #5D6975;
          font-size: 2.4em;
          line-height: 1.4em;
          font-weight: normal;
          text-align: center;
          margin: 0 0 20px 0;
          background: url(dimension.png);
        }

        #project {
          float: left;
        }

        #project span {
          color: #5D6975;
          text-align: right;
          width: 52px;
          margin-right: 10px;
          display: inline-block;
          font-size: 0.8em;
        }

        #company {
          float: right;
          text-align: right;
        }

        #project div,
        #company div {
          white-space: nowrap;        
        }

        table {
          width: 100%;
          border-collapse: collapse;
          border-spacing: 0;
          margin-bottom: 20px;
        }

        table tr:nth-child(2n-1) td {
          background: #F5F5F5;
        }

        table th,
        table td {
          text-align: center;
        }

        table th {
          padding: 5px 20px;
          color: #5D6975;
          border-bottom: 1px solid #C1CED9;
          white-space: nowrap;        
          font-weight: normal;
        }

        table .service,
        table .desc {
          text-align: left;
        }

        table td {
          padding: 20px;
          text-align: right;
        }

        table td.service,
        table td.desc {
          vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
          font-size: 1.2em;
        }

        table td.grand {
          border-top: 1px solid #5D6975;;
        }

        #notices .notice {
          color: #5D6975;
          font-size: 1.2em;
        }

        footer {
          color: #5D6975;
          width: 100%;
          height: 30px;
          position: absolute;
          bottom: 0;
          border-top: 1px solid #C1CED9;
          padding: 8px 0;
          text-align: center;
        }
        .logo_work-up{
          width:40px;
          height:40px;
          text-align: center;

        }
        </style></head>
          <body>
            <header class="clearfix">
              <h1>Nom COMPAGNIE</h1>
              <h2>Numéro Devis :</h2>
              <div id="company" class="clearfix">
                <div>Company Name</div>
                <div>455 Foggy Heights,<br> AZ 85004, US</div>
                <div>(602) 519-0450</div>
                <div><a href="mailto:company@example.com">company@example.com</a></div>
              </div>
              <div id="project">
                <div><span>PROJET</span> Website development</div>
                <div><span>CLIENT</span> John Doe</div>
                <div><span>ADRESSE</span> 796 Silver Harbour, TX 79273, US</div>
                <div><span>EMAIL</span> <a href="mailto:john@example.com">john@example.com</a></div>
                <div><span>DATE</span> August 17, 2015</div>
                <div><span>DATE DE FIN</span> September 17, 2015</div>
              </div>
            </header>
            <main>
              <table>
                <thead>
                  <tr>
                    <th class="desc">LIBELLE</th>
                    <th>PRIX Uni.</th>
                    <th>QTE</th>
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="desc">Creating a recognizable design solution based on the companys existing visual identity</td>
                    <td class="unit">$40.00</td>
                    <td class="qty">26</td>
                    <td class="total">$1,040.00</td>
                  </tr>
                  <tr>
                    <td class="desc">Developing a Content Management System-based Website</td>
                    <td class="unit">$40.00</td>
                    <td class="qty">80</td>
                    <td class="total">$3,200.00</td>
                  </tr>
                  <tr>
                    <td class="desc">Optimize the site for search engines (SEO)</td>
                    <td class="unit">$40.00</td>
                    <td class="qty">20</td>
                    <td class="total">$800.00</td>
                  </tr>
                  <tr>
                    <td class="desc">Initial training sessions for staff responsible for uploading web content</td>
                    <td class="unit">$40.00</td>
                    <td class="qty">4</td>
                    <td class="total">$160.00</td>
                  </tr>
                  <tr>
                    <td colspan="4">HT</td>
                    <td class="total">$5,200.00</td>
                  </tr>
                  <tr>
                    <td colspan="4">TVA 19,6%</td>
                    <td class="total">$1,300.00</td>
                  </tr>
                  <tr>
                    <td colspan="4" class="grand total">TOTAL TTC</td>
                    <td class="grand total">$6,500.00</td>
                  </tr>
                </tbody>
              </table>
              <div id="notices">
                <div>NOTICE:</div>
                <div class="notice">Nous restons à votre disposition pour toute information complémentaire.</div>
                <div class="notice">Si ce devis vous convient, veuillez nous retourner signé précédé de la mention :</div>
                <div class="notice"><b>"BON POUR ACCORD ET EXECUTION DU DEVIS"</b></div>
                <br>
                <div class="date_signature">Date et Signature :</div>
              </div>
            </main>
            <footer>
              <p>Ce devis a été généré par la société work-up.fr</p>
              <img class="logo_work-up" src="theme/theme-work-up/public/img/logo/Mobile_Icone.png"/>
            </footer>
          
        </body></html>
        ');
        return $pdf->stream();
    }

    public function generateQuotation() {
     if (Input::has('user_id')) {
      $userId = Input::get('user_id');
        if (Input::has('quotation_id')) {
          $quotationId = Input::get('quotation_id');
          $user = User::find($userId);
          $quotation = Quotation::find($quotationId);
          $book = array();
          $dateDebut = Input::get('datedebut');
          $dateFin = Input::get('datefin');
          foreach(Input::all() as $k => $v) {
           if (strpos($k, 'book_') !== false) {
              $index = substr($k, strrpos($k, '_') + 1);
              $index = (int) $index;

             if (isset($book[$index])) {
               $book[$index] = array_merge($book[$index], array((substr($k, strpos($k, "_")+4, strpos($k, "_")-strlen($k))) => $v));
              } else {
                $book[$index] = array((substr($k, strpos($k, "_")+4, strpos($k, "_")-strlen($k))) => $v);
            }
          }
        }
        //return var_dump($book);
           $html = '<!DOCTYPE html>
           <!-- saved from url=(0105)https://s3-eu-west-1.amazonaws.com/htmlpdfapi.production/free_html5_invoice_templates/example1/index.html -->
          <html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta charset="utf-8">
            <title>Work-Up Devis</title>
            <link rel="stylesheet" href="./Example 1_files/style.css" media="all">
          <style>.clearfix:after {
          content: "";
          display: table;
          clear: both;
        }

        a {
          color: #5D6975;
          text-decoration: underline;
        }

        body {
          position: relative;
          width: 100%;  
          height: 100%; 
          margin: 0 auto; 
          color: #001028;
          background: #FFFFFF; 
          font-family: Arial, sans-serif; 
          font-size: 12px; 
          font-family: Arial;
        }

        header {
          padding: 10px 0;
          margin-bottom: 30px;
        }

        #logo {
          text-align: center;
          margin-bottom: 10px;
        }

        #logo img {
          width: 90px;
        }

        h1 {
          border-top: 1px solid  #5D6975;
          border-bottom: 1px solid  #5D6975;
          color: #5D6975;
          font-size: 2.4em;
          line-height: 1.4em;
          font-weight: normal;
          text-align: center;
          margin: 0 0 20px 0;
          background: url(dimension.png);
        }

        #project {
          float: left;
        }

        #project span {
          color: #5D6975;
          text-align: left;
          width: 52px;
          margin-right: 10px;
          display: inline-block;
          font-size: 0.8em;
        }

        #company {
          float: right;
          text-align: right;
        }

        #project div,
        #company div {
          white-space: nowrap;        
        }

        table {
          width: 100%;
          border-collapse: collapse;
          border-spacing: 0;
          margin-bottom: 20px;
        }

        table tr:nth-child(2n-1) td {
          background: #F5F5F5;
        }

        table th,
        table td {
          text-align: center;
        }

        table th {
          padding: 5px 20px;
          color: #5D6975;
          border-bottom: 1px solid #C1CED9;
          white-space: nowrap;        
          font-weight: normal;
        }

        table .service,
        table .desc {
          text-align: left;
        }

        table td {
          padding: 20px;
          text-align: right;
        }

        table td.service,
        table td.desc {
          vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
          font-size: 1.2em;
        }

        table td.grand {
          border-top: 1px solid #5D6975;;
        }

        #notices .notice {
          color: #5D6975;
          font-size: 1.2em;
        }

        footer {
          color: #5D6975;
          width: 100%;
          height: 30px;
          position: absolute;
          bottom: 0;
          border-top: 1px solid #C1CED9;
          padding: 8px 0;
          text-align: center;
        }
        .logo_work-up{
          width:40px;
          height:40px;
          text-align: center;

        }
        </style></head>
          <body>
            <header class="clearfix">
              <h1>'. $quotation->getBenefitInfos()->getRedactorCompagny().'</h1>
              <div id="company" class="clearfix">
                <div>'. $quotation->getBenefitInfos()->getRedactorName() .'</div>
                <div>' .$quotation->getBenefitInfos()->getRedactorAdress() . '</div>
                <div>(602) 519-0450</div>
                <div><a href="mailto:'. $quotation->getBenefitInfos()->getRedactorMail(). '">' . $quotation->getBenefitInfos()->getRedactorMail() . '</a></div>
              </div>
              <div id="project">
                <div><span>PROJET</span>' . $quotation->getBenefitInfos()->title . '</div>
                <div><span>CLIENT</span>'. $quotation->getBenefitInfos()->getDestinationName() . '</div>
                <div><span>ADRESSE</span>' . $quotation->getBenefitInfos()->getDestinationAdress() . '</div>
                <div><span>EMAIL</span> <a href="mailto:' . $quotation->getBenefitInfos()->getDestinationMail(). '">' .$quotation->getBenefitInfos()->getDestinationMail() .'</a></div>
                </br>
                </br>
                <div><span>DATE DE DEBUT DE LA PRESTATION</span></div>
                <div>'. $dateDebut .'</div>
                <div><span>DATE DE FIN DE LA PRESTATION</span></div>
                <div>'. $dateFin .'</div>
              </div>
            </header>
            <main>
               <table>
                <thead>
                  <tr>
                    <th class="desc">LIBELLE</th>
                    <th>PRIX Uni.</th>
                    <th>QTE</th>
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody>';

                $tot = 0;
                foreach($book as $b) {
                 
                  $html .= 
                  '<tr>
                    <td class="desc">'. $b[0] .'</td>
                    <td class="unit">'. $b[1] .'&#8364</td>
                    <td class="qty">'. $b[2] .'</td>
                    <td class="total">'. $b[1] * $b[2] .'&#8364</td>
                  </tr>';
                  $tot += $b[1] * $b[2];
                }
                $tva = round($tot / 19.6 , 2);
                $grandTot = $tot + $tva ;
                $html .= 
                  '<tr>
                    <td colspan="3">HT</td>
                    <td class="total">'. $tot .'&#8364</td>
                  </tr>
                  <tr>
                    <td colspan="3">TVA 19,6%</td>
                    <td class="total">'. $tva . '&#8364</td>
                  </tr>
                  <tr>
                    <td colspan="3" class="grand total">TOTAL TTC</td>
                    <td class="grand total">'. $grandTot . '&#8364</td>
                  </tr>
                </tbody>
              </table>
              <div id="notices">
                <div>NOTICE:</div>
                <div class="notice">Nous restons &#224 votre disposition pour toute information compl&#233mentaire.</div>
                <div class="notice">Si ce devis vous convient, veuillez nous retourner sign&#233 pr&#233c&#233d&#233 de la mention :</div>
                <div class="notice"><b>"BON POUR ACCORD ET EXECUTION DU DEVIS"</b></div>
                <br>
                <div class="date_signature">Date et Signature :</div>
              </div>
            </main>
            <footer>
              <p>Ce devis a &#233t&#233 g&#233n&#233r&#233 par la soci&#233t&#233 work-up.fr</p>
              <img class="logo_work-up" src="http://work-up.fr/theme/theme-work-up/public/img/logo/Mobile_Icone.png"/>
            </footer>
          
        </body></html>';
     }
     }
      if (isset($_POST['visualiser'])) {
       $quotation->data_html = $html;
       $quotation->begin_date = $dateDebut;
       $quotation->end_date = $dateFin;
       $quotation->file_name = 'devis' . $quotation->id;
       $quotation->save();
       //change state of quotation
       $brq = BenefitRequestQuotation::where('quotation_id',$quotation->id)->first();
       $brq->state = 'en cours';
       $brq->save();
       $pdf = App::make('dompdf');
       $pdf->loadHTML($html);
       $output = $pdf->output();
       return $pdf->download('devis'. $quotation->id .'.pdf');
    } else return 'error';
  }

  public function dlQuotation() {
    if (Input::has('user_id')) {
      $userId = Input::get('user_id');
        if (Input::has('quotation_id')) {
          $quotationId = Input::get('quotation_id');
          $user = User::find($userId);
          $quotation = Quotation::find($quotationId);
        }
        if (isset($_POST['visualiser'])) {
          $html = $quotation->data_html;
          $pdf = App::make('dompdf');
          $pdf->loadHTML($html);
          $output = $pdf->output();
          return $pdf->download('devis'. $quotation->id .'.pdf');
        } elseif (isset($_POST['valider'])) {
            $brq = BenefitRequestQuotation::where('quotation_id',$quotation->id)->first();
            $brq->state = 'accepte';
            $brq->save();
            return Redirect::action('BenefitController@getDevisPage', $user->slug);

        } elseif (isset($_POST['refuser'])) {
            $brq = BenefitRequestQuotation::where('quotation_id',$quotation->id)->first();
            $brq->state = 'refuse';
            $brq->save();
            return Redirect::action('BenefitController@getDevisPage', $user->slug);

        } else return 'error';
      }
    }
}