<?php

class BenefitController extends ModuleWorkUpController 
{
	 public function getBenefitPage($slug) {
        $user = User::where('slug',$slug)->first();
        if(!empty($user)) {
        	if (Auth::check()){
        		$auth = Auth::user();
        		if (!empty($auth)) {
        			$authUser = DB::table('auth_user')->where('auth_id', $auth->id)->first(array('user_id'));
	        		if (!empty($authUser)) {
	        			$current_user = User::where('id',$authUser->user_id)->first();
	        			if ($user->id != $current_user->id) {
	        				return View::make('theme::public.error.404',$this->data); 
	        			}
	        		}
        			$this->data['user'] = $user;
        			$ae = AutoContractor::where('user_id',$user->id)->first();
        			if (!empty($ae)) {
		                $benefitRequestEnCours = $ae->getBenefitRequest('en cours');
		                $NbBenefitRequestEnCours = $ae->getNbBenefitRequest('en cours');
		                $benefitRequestAccepte = $ae->getBenefitRequest('accepte');
		                $benefitRequestRefuse = $ae->getBenefitRequest('refuse');
        				$benefitRequestNegociation = $ae->getBenefitRequest('negociation');
        			} else {
        				    $benefitRequestEnCours = $user->getBenefitRequest('en cours');
                    $NbBenefitRequestEnCours = $user->getNbBenefitRequest('en cours');
		                $benefitRequestAccepte = $user->getBenefitRequest('accepte');
		                $benefitRequestRefuse = $user->getBenefitRequest('refuse');
		                $benefitRequestNegociation = $user->getBenefitRequest('negociation');
        			}
              $this->data['requestEnCours'] = $benefitRequestEnCours;
              $this->data['requestNbEnCours'] = $NbBenefitRequestEnCours;
              $this->data['requestAcceptes'] = $benefitRequestAccepte;
              $this->data['requestRefuses'] = $benefitRequestRefuse;
        			$this->data['requestNegociations'] = $benefitRequestNegociation;
        			return View::make('theme::public.user.benefit.global-benefit',$this->data);	
	        		}
	        	}
        	}
        return View::make('theme::public.error.404');    
    }

     public function getPrestationPage($slug) {
        $user = User::where('slug',$slug)->first();
        if(!empty($user)) {
        	if (Auth::check()){
        		$auth = Auth::user();
        		if (!empty($auth)) {
        			$authUser = DB::table('auth_user')->where('auth_id', $auth->id)->first(array('user_id'));
	        		if (!empty($authUser)) {
	        			$current_user = User::where('id',$authUser->user_id)->first();
	        			if ($user->id != $current_user->id) {
	        				return View::make('theme::public.error.404',$this->data); 
	        			}
	        		}
        			$this->data['user'] = $user;
              $ae = AutoContractor::where('user_id',$user->id)->first();
              if (!empty($ae)) {
                    $benefitRequestAccepte = $ae->getBenefitRequest('accepte');
              } else {
                    $benefitRequestAccepte = $user->getBenefitRequest('accepte');
              }
                  $this->data['requestAcceptes'] = $benefitRequestAccepte;
        			return View::make('theme::public.user.benefit.prestation',$this->data);	
	        		}
	        	}
        	}
        return View::make('theme::public.error.404');    
    }

         public function getDevisPage($slug) {
        $user = User::where('slug',$slug)->first();
        if(!empty($user)) {
        	if (Auth::check()){
        		$auth = Auth::user();
        		if (!empty($auth)) {
        			$authUser = DB::table('auth_user')->where('auth_id', $auth->id)->first(array('user_id'));
	        		if (!empty($authUser)) {
	        			$current_user = User::where('id',$authUser->user_id)->first();
	        			if ($user->id != $current_user->id) {
	        				return View::make('theme::public.error.404',$this->data); 
	        			}
	        		}
        			$this->data['user'] = $user;
              $ae = AutoContractor::where('user_id',$user->id)->first();
              if (!empty($ae)) {
                    $quotationAccepte = $ae->getQuotation('accepte');
                    $quotationEnCours = $ae->getQuotation('en cours');
                    $quotationAFaire = $ae->getQuotation('a faire');
                    $quotationRefuse = $ae->getQuotation('refuse');
              } else {
                    $quotationAccepte = $user->getQuotation('accepte');
                    $quotationEnCours = $user->getQuotation('en cours');
                    $quotationAFaire = $user->getQuotation('a faire');
                    $quotationRefuse = $user->getQuotation('refuse');
              }
                  $this->data['quotationAcceptes'] = $quotationAccepte;
                  $this->data['quotationEncours'] = $quotationEnCours;
                  $this->data['quotationAFaires'] = $quotationAFaire;
                  $this->data['quotationRefuses'] = $quotationRefuse;
        			return View::make('theme::public.user.benefit.devis',$this->data);	
	        		}
	        	}
        	}
        return View::make('theme::public.error.404');    
    }


    public function postAskBenefit() {
      if (Input::has('user')) {
        $user = Input::get('user');
      }
      if (Input::has('userPublic')) {
        $userPublic = Input::get('userPublic');
        $ae = AutoContractor::where('user_id',$userPublic)->first();
      }
      $br = BenefitRequest::where('auto_contractor_id',$ae->id)->where('user_id',$userPublic)->first();
      if (!empty($br)){
        if (Request::ajax()) {
            return Response::json(array('statut' => 'danger', 'message' => 'Vous avez déjà une demande en cours avec cet auto-entrepreneur, veuillez patienter...'));
        } else {
          return Redirect::back()->withInput()->with('error', 'Vous avez déjà une demande en cours avec cet auto-entrepreneur, veuillez patienter...');
        }
      }
    	if (Input::has('description')) {
    	  $description = Input::get('description');
   		} else {
        if (Request::ajax()) {
          return Response::json(array('statut' => 'danger', 'message' => 'Il faut décrire votre demande !'));
      } else 
            {
          return Redirect::back()->withInput()->with('error', 'Il faut décrire votre demande !');
      }

      }
   		if (Input::has('date')) {
    	  	$format = 'd-m-Y';
			     $date = DateTime::createFromFormat($format, str_replace('/','-',Input::get('date')));
   		}
   		if (Input::has('budgetPrestation')) {
    	  $budgetPrestation = Input::get('budgetPrestation');
       } else {
        if (Request::ajax()) {
          return Response::json(array('statut' => 'danger', 'message' => 'Veuillez renseigner une estimation de votre prestation future'));
        } else {
          return Redirect::back()->withInput()->with('error', 'Veuillez renseigner une estimation de votre prestation future');
        }
   		}
   		if (Input::has('ville')) {
    	  $ville = Input::get('ville');
   		}
   		if (Input::has('titre')) {
    	  $titre = Input::get('titre');
   		} else 
        $titre = null;

   		//Check si il y a deja une prestation en cours entre les deux parties
   		$response = BenefitRequest::add($titre,$description,$date,$budgetPrestation,$ville,$user,$userPublic);
   		if (!$response){
   			if (Request::ajax()) {
				  return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
			} else 
            {
				  return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
			}
   		} else {
   			$userReturn = User::where('id',$userPublic)->first();
   			if (Request::ajax()) {
				return Response::json(array('statut' => 'success', 'message' => 'Votre demande a bien été envoyé!'));
			} else {
				return Redirect::action('ProfilController@getPage', $userReturn->slug);
   		}	
    }
  }

  public function postAskBenefitRequest() {
    if (Input::has('user')) {
        $user = Input::get('user');
    }
     $br = BenefitRequest::find(Input::get('prestation_id'));
    if(isset($_POST['valider'])) {
      $br->state = 'accepte';
      $quot = new Quotation();
      $quot->add($br);
      $message = 'coucou';
      $kk = GcmIds::send($message,$user);
        }
      
   if(isset($_POST['refuser'])) {
    $br->state = 'refuse';
   }
   if (!$br->save()) {
        if (Request::ajax()) {
           return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
        } else {
          return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
        }
      }
    $userReturn = User::where('id',$user)->first();
    if (Request::ajax()) {
        return Response::json(array('statut' => 'success', 'message' => 'Votre demande a bien été envoyé!'));
      } else {
        return Redirect::action('BenefitController@getBenefitPage', $userReturn->slug);
      } 
  }

  public function postAskMoreInformations() {
     if (Input::has('user')) {
        $user = Input::get('user');
    }
    if (Input::has('text')) {
        $message = Input::get('text');
    } else {
      if (Request::ajax()) {
           return Response::json(array('statut' => 'danger', 'message' => 'Veuillez mettre un message !'));
        } else {
          return Redirect::back()->withInput()->with('error', 'Veuillez mettre un message !');
        }
    }
    $br = BenefitRequest::find(Input::get('prestation_id'));
    $br->state = 'negociation';
    if (!$br->save()) {
        if (Request::ajax()) {
           return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
        } else {
          return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
        }
      }
    if (BenefitRequestDetails::addMessage($message,$br->id)) {
      $userReturn = User::where('id',$user)->first();
      if (Request::ajax()) {
        return Response::json(array('statut' => 'success', 'message' => 'Votre demande a bien été envoyé!'));
      } else {
        return Redirect::action('BenefitController@getBenefitPage', $userReturn->slug);
      } 
    }
  }

  public function postResponseMoreInformations() { 
    if (Input::has('user')) {
        $user = Input::get('user');
    } if (Input::has('text-response')) {
        $response = Input::get('text-response');
    } else {
      if (Request::ajax()) {
          return Response::json(array('statut' => 'danger', 'message' => 'Veuillez mettre un message !'));
      } else {
          return Redirect::back()->withInput()->with('error', 'Veuillez mettre un message !');
      }
    }
    $br = BenefitRequest::find(Input::get('prestation_id'));
    $br->state = 'en cours';
    if (!$br->save()) {
      if (Request::ajax()) {
         return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
      } else {
        return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
      }
    }
    if (BenefitRequestDetails::addResponse($response,$br->id)) {
      $userReturn = User::where('id',$user)->first();
      if (Request::ajax()) {
        return Response::json(array('statut' => 'success', 'message' => 'Votre demande a bien été envoyé!'));
      } else {
        return Redirect::action('BenefitController@getBenefitPage', $userReturn->slug);
      }
    } 
  }

}