<?php

class NotifController extends ModuleWorkUpController
{
    

    public function mGetNotifications() {
        if (Input::has('id')) {
            $id = Input::get('id');
            $notifications = GcmIds::where('user_id',$id)->get();
            $output = array();
            $i = 0;
            foreach ($notifications as $notification) {
                $output[$i]['id'] = $notification->id;
                $output[$i]['title'] = $notification->title;
                $output[$i]['description'] = $notification->description;
                $output[$i]['user_id'] = $notification->user_id;
                $output[$i]['remember_token'] = $notification->remember_token;
                $output[$i]['last_visit_at'] = $notification->last_visit_at;
                $output[$i]['created_at'] = $notification->created_at;
                $i++;
            }
             $data = Response::json([$output])->header('Content-Type', 'application/json');
             echo $data->getContent();
             exit(); 
        }
        $output = array();
        $output['response'] = 'false';
        $output['error'] = 'parametre(s) manquant(s)';
        $data = Response::json([$output])->header('Content-Type', 'application/json');
        echo $data->getContent();
        exit();
    }

    public function register($gcm_token, $idUser) {
            $res = GcmIds::add(urldecode($gcm_token),$idUser);
            $response = array();
            if ($res) {
                $response['response'] = 'true';
            }
            else{
                $response['response'] = 'false';
            }
            $data = Response::json([$response])->header('Content-Type', 'application/json');
            echo $data->getContent();
            exit();
    }
}