<?php

class ModuleWorkUpRegisterController extends ModuleWorkUpController 
{

	public $validExtention = array('png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG');

	public function create() {
		return View::make('theme::public.user.sign-up');
	}

	public function createAe(){
		return View::make('theme::public.user.sign-up-ae');
	}

	public function createPassword(){
		return View::make('theme::public.user.forgot-password');
	}

	public function createProfilPicture() {
		return View::make('theme::public.user.components.profil-picture-form');
	}

	public function forgotPassword() {
		$validator = Validator::make(Input::all(), Config::get('workup::validator.sign-up'));
		if ($validator->passes()) 
		{
			// Test if the email exist in BDD
			$user_mail = AuthUser::where('mail','=',Input::get('email'))->first();
			if (empty($user_mail)) {
				if (Request::ajax()) {
					return Response::json(array('statut' => 'danger', 'message' => 'Email inexistant !'));
				} else {
					return Redirect::back()->with('error', 'Email inexistant !');
				}
			}
			$auth = AuthUser::where('email',$user_mail->mail)->first();
			//envoi de mail
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPAuth   = true;
	        $mail->Host       = "smtp.gmail.com";
	        $mail->Port       = 587;
	        $mail->Username   = "Workup.Project@gmail.com";
	        $mail->Password   = "kLd2KMMx";
	        $mail->smtpConnect( array(
	        	"ssl" => array(
	            "verify_peer" => false,
	            "verify_peer_name" => false,
	            "allow_self_signed" => true
        		)
    		));
	        $mail->SMTPSecure = 'tls';                                    
			$mail->SetFrom('Workup.Project@gmail.com','Work-Up');
			$mail->FromName = 'Client Work-Up';
			$mail->addAddress(Input::get('email'));         
			$mail->isHTML(false);                                  
			$mail->Subject = 'Procedure de recuperation de mot de passe';
			$mail->Body    = "Votre mot de passe pour l'application Work-Up :'". $auth->password ."'";
			$mail->AltBody = 'Work-Up vous remercie pour votre intérêt ! :)';
			if(!$mail->send()) {
				if (Request::ajax()) {
					return Response::json(array('statut' => 'danger', 'message' => 'Problème lors de l\'envoi du mail !'));
				} else {
					return Redirect::back()->with('error', 'Problème lors de l\'envoi du mail !');
				}
			}	
			if (Request::ajax()) {
				return Response::json(array('statut' => 'success', 'message' => 'Un mail avec votre mot de passe vous a été envoyé ! :)'));
			} else {
				return Redirect::route('/');
			}
		}
		if (Request::ajax()) {
			return Response::json(array('statut' => 'danger', 'message' => $validator->messages()->all())); 
		} else {
			return Redirect::back()->withInput()->withErrors($validator);
		}	
	}


	// For add the similar information for clients and auto e 
	public function add() {
		// Validate inputs
		$validator = Validator::make(Input::all(), Config::get('workup::validator.sign-up'));
		if ($validator->passes()) 
		{
			// Test if the email exist in BDD
			$user_mail = User::where('mail','=',Input::get('email'))->first();
			if (!empty($user_mail)) {
				if (Request::ajax()) {
					return Response::json(array('statut' => 'danger', 'message' => 'Email déjà utilisé !'));
				} else {
					return Redirect::back()->withInput()->with('error', 'Email déjà utilisé !');
				}
			}
			//Siren traitement
			if (Input::get('typeInscription') == 'A') {
				if (AutoContractor::where('siren',Input::get('siren'))->first()) {
					if (Request::ajax()) {
						return Response::json(array('statut' => 'danger', 'message' => 'SIREN déjà utilisé !'));
					} else {
						return Redirect::back()->withInput()->with('error', 'SIREN déjà utilisé !');
					}
				} else {
					if (strlen(Input::get('siren')) != 9 ) {
						if (Request::ajax()) {
							return Response::json(array('statut' => 'danger', 'message' => 'Le n° de SIREN doit comporter 9 caractères !'));
						} else {
							return Redirect::back()->withInput()->with('error', 'Le n° de SIREN doit comporter 9 caractères !');
						}
					}
				}
				if (Input::has('categorie_metier')) {
					if (Input::get('categorie_metier')  != 0) {
						$jobCategory_id = Input::get('categorie_metier');
					} else {
						if (Request::ajax()) {
							return Response::json(array('statut' => 'danger', 'message' => 'Veuillez sélectionner une catégorie de métier !'));
						} else {
							return Redirect::back()->withInput()->with('error', 'Veuillez sélectionner une catégorie de métier !');
						}
					}
				}
				if (Input::has('compagny')) {
					$compagny = Input::get('compagny');
				}	
			}
			if (Input::get('password') == Input::get('vpassword')) {
				//Add user informations
				$user = new User();
				if (Input::has('nom')) {
					$user->last_name = Input::get('nom');
				}
				if (Input::has('prenom')) {
					$user->first_name = Input::get('prenom');
				}
				if (Input::has('email')) {
					$user->mail = Input::get('email');
				}
				if (Input::has('phone')) {
					$user->phone = Input::get('phone');
				}
				if (Input::has('adress')) {
					$user->address = Input::get('adress');
				}
				if (Input::has('postal_code')) {
					$user->postal_code = Input::get('postal_code');
				}
				if (Input::has('city')) {
					$user->city = Input::get('city');
				}
				if (Input::has('date')) {
					$format = 'd-m-Y';
					$date = DateTime::createFromFormat($format, str_replace('/','-',Input::get('date')));
					$user->birthday = $date;
				}
				if (Input::has('siren')) {
						$siren = Input::get('siren');
				}
				if (Input::get('sexe') != '') {
					if (Input::get('sexe') == 'male' ) {
						$user->isMale = true;
					}
					else { $user->isMale = false; }
				}
				$slug = strtolower($user->last_name) . '.' . strtolower($user->first_name);
		        if(!User::where('slug',$slug)->first()) {
		            $user->slug = $slug;
		        } else {
		            $user->slug = $slug . uniqid();
       			 }
				if (!$user->save()) {
					if (Request::ajax()) {
						return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de l\'enregistrement !'));
					} else {
						return Redirect::back()->withInput()->with('error', 'Erreur lors de l\'enregistrement !');
					}
				}
				//ADD client or Auto E
				if (Input::get('typeInscription') == 'C') {
					Client::add($user);
				} else {
					
					$autoE = AutoContractor::add($user,$siren,null,$compagny);
					//Attach Job Category to Auto Contractor
					$autoE->attachJobCategory($jobCategory_id);
				}
				//Create AUTH
				$auth = AuthUser::add(Input::get('email'), Input::get('password'));

				if (!empty($user) && !empty($auth)) {
					User::find($user->id)->auths()->save($auth);
					Auth::loginUsingId($auth->id);
					if (Request::ajax()) {
						return Response::json(array('statut' => 'success', 'message' => 'Vous êtes maintenant inscrit sur WorkUp ! :)', 'user' => $user));
					} else {
						return Redirect::route('homepage');
					}
				}
			}
			if (Request::ajax()) {
				return Response::json(array('statut' => 'danger', 'message' => 'Les mots de passe sont différents !'));
			} else {
				return Redirect::back()->withInput()->with('error', 'Les mots de passe sont différents !');
			}
		}
		if (Request::ajax()) {
			return Response::json(array('statut' => 'danger', 'message' => $validator->messages()->all()));
		} else {
			return Redirect::back()->withInput()->withErrors($validator);
		}
	}

	public function postImages() {
		if (Input::hasFile('image')) {
			// Get File
			$file = Input::file('image');
			// Make fileName
			$pathInfo = pathinfo($file->getClientOriginalName());
			$fileName = Helpers::makeFileName('rand');
			$fileExt = $pathInfo['extension'];

			// Check size of files
			if ($file->getClientSize() > 2096576) {
				if (Request::ajax()) {
					return Response::json(['statut'=>'danger', 'message' => 'Votre photo est trop volumineuse... Elle doit faire moins de 2Mo !']);
				} else {
					return Redirect::back()->with(array('error' => 'Votre photo est trop volumineuse... Elle doit faire moins de 2Mo !'));
				}
			}

			// Check extention
			if (!in_array($fileExt, $this->validExtention)) {
				if (Request::ajax()) {
					return Response::json(['statut'=>'danger', 'message' => 'Extention de fichier non authorisée !']);
				} else {
					return Redirect::back()->with(array('error' => 'Extention de fichier non authorisée !'));
				}
			}

			// Make filePath
			$filePath = 'uploads/user/' . Input::get('user_id') . '/profil-picture/';
			// Move file to storage
			$file->move($filePath, $fileName . '.' . $pathInfo['extension']);
		} else {
			if (Request::ajax()) {
				return Response::json(['statut'=>'danger', 'message' => 'Fichier introuvable...']);
			} else {
				return Redirect::back()->with(array('error' => 'Fichier introuvable...'));
			}
		}
		// Add Image
		$image = WorkUpImage::add($fileName,$fileExt);
		
		//Check if User had already Image
		$userIm = UserImage::where('user_id',Input::get('user_id'))->where('active',true)->where('type','profil')->first();
		if (!empty($userIm)) {
			$userIm->active = false;
			$userIm->save();
		}
		//attach image to user
		$userImage = new UserImage();
		$userImage->user_id = Input::get('user_id');
		$userImage->image_id = $image->id;
		$userImage->type = 'profil';
		if (!$userImage->save()) {
			if (Request::ajax()) {
				return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de la liaison de l\'image !'));
			} else {
				return Redirect::back()->with('error', 'Erreur lors de la liason de l\'image !');
			}
		}
		$userReturn = User::where('id',Input::get('user_id'))->first();
		return Redirect::action('ProfilController@getPage', $userReturn->slug); 
	}

	public function postCoverImage() {
		if (Input::hasFile('image')) {
			// Get File
			$file = Input::file('image');
			// Make fileName
			$pathInfo = pathinfo($file->getClientOriginalName());
			$fileName = Helpers::makeFileName('rand');
			$fileExt = $pathInfo['extension'];

			// Check size of files
			if ($file->getClientSize() > 2096576) {
				if (Request::ajax()) {
					return Response::json(['statut'=>'danger', 'message' => 'Votre photo est trop volumineuse... Elle doit faire moins de 2Mo !']);
				} else {
					return Redirect::back()->with(array('error' => 'Votre photo est trop volumineuse... Elle doit faire moins de 2Mo !'));
				}
			}

			// Check extention
			if (!in_array($fileExt, $this->validExtention)) {
				if (Request::ajax()) {
					return Response::json(['statut'=>'danger', 'message' => 'Extention de fichier non authorisée !']);
				} else {
					return Redirect::back()->with(array('error' => 'Extention de fichier non authorisée !'));
				}
			}

			// Make filePath
			$filePath = 'uploads/user/' . Input::get('user_id') . '/cover-picture/';
			// Move file to storage
			$file->move($filePath, $fileName . '.' . $pathInfo['extension']);
		} else {
			if (Request::ajax()) {
				return Response::json(['statut'=>'danger', 'message' => 'Fichier introuvable...']);
			} else {
				return Redirect::back()->with(array('error' => 'Fichier introuvable...'));
			}
		}
		// Add Image
		$image = WorkUpImage::add($fileName,$fileExt);
		//Check if User had already Image
		$userIm = UserImage::where('user_id',Input::get('user_id'))->where('active',true)->where('type','couverture')->first();
		if (!empty($userIm)) {
			$userIm->active = false;
			$userIm->save();
		}
		//attach image to user
		$userImage = new UserImage();
		$userImage->user_id = Input::get('user_id'); 
		$userImage->image_id = $image->id;
		$userImage->type = 'couverture';
		if (!$userImage->save()) {
			if (Request::ajax()) {
				return Response::json(array('statut' => 'danger', 'message' => 'Erreur lors de la liaison de l\'image !'));
			} else {
				return Redirect::back()->with('error', 'Erreur lors de la liason de l\'image !');
			}
		}
		$userReturn = User::where('id',Input::get('user_id'))->first();
		if (Request::ajax()) {
			return Response::json(array('statut' => 'success', 'message' => 'Votre image a bien été enregistré :)', 'user_id' => $user->id));
		} else {
			return Redirect::action('ProfilController@getPage', $userReturn->slug);
		} 
	}

	public function mpostImage () {

		if (Input::has('id')) {
			$id = Input::get('id');
			if (Input::has('type')) {
				$type = Input::get('type');
			} 
			if (Input::has('fileExt')) {
				$fileExt = Input::get('fileExt');
			}
		} else {
			$output = array();
			$output['response'] = 'false';
			$output['error'] = 'problem with post parameters';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();	
		} 


		$fileName = Helpers::makeFileName('rand');
		if($type == 'couverture') {
			if (!file_exists('/var/www/dynamix/current/public/uploads/user/' . $id)) {
				File::makeDirectory('/var/www/dynamix/current/public/uploads/user/' . $id, 0777, true, true);
			}
			if (!file_exists('/var/www/dynamix/current/public/uploads/user/' . $id . '/cover-picture/')) {
				File::makeDirectory('/var/www/dynamix/current/public/uploads/user/' . $id . '/cover-picture/', 0777, true, true);
			}
			$filePath = '/var/www/dynamix/current/public/uploads/user/' . $id . '/cover-picture/';
		} else {
			if (!file_exists('/var/www/dynamix/current/public/uploads/user/' . $id . '/profil-picture/')) {
				File::makeDirectory('/var/www/dynamix/current/public/uploads/user/' . $id . '/profil-picture/', 0777, true, true);
			}
			if (!file_exists('/var/www/dynamix/current/public/uploads/user/' . $id . '/profil-picture/')) {
				File::makeDirectory('/var/www/dynamix/current/public/uploads/user/' . $id . '/profil-picture/', 0777, true, true);
			}
			$filePath = '/var/www/dynamix/current/public/uploads/user/' . $id . '/profil-picture/';
		}

		//Construct File From Data Image
		if (Input::has('data_image')) {
			$data = Input::get('data_image');
		} else {
			$data = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJkAAADMCAIAAADvfZapAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4
						nHS9WY90WXYdttY+NyIy8xtr7K5mN0m3OJp0k7JFyjZAGJBEmTYlGLB/jn+B/4T8YlkvMkDYhocH
						G5AFGrJsiqRJdaPZbPVAsseqruEbMjPinr38sPY592YVnSCr84uMuHHvOXtce+19+E9+779bREpd
						AhBAFxsFQOoAMjOJEABEhEQlwSRJIdEBAAgQQaQkkUSoQ4c4ZKakiCAyRdVPDzIlKggA6JkigOj9
						QooKkpBSQlC5klSSUZ8nmi9IEoDGbfhPAMAk0XvdpwgKCCDhbxK6X4cCoVRXRov6TpKZSUKoew5S
						Ekhl+q9CAOkbAHsmkJDkR87s5/Md1DNT6iIaQyIAklKP+kWkb1dMRQTANRNANIYAMAFJAXRksAkp
						iak1RWXv/XA4iLEc4kCszMD4ABBSB9dQIMnWmAlC6r33iAXMiFCS9JcByNrRCElkE9em8C4CSTER
						EcwE0IEWDOlCeNXWiNaVyL5EA5BKsqVWsglJNKkzAADzS4OUkCClsS4BiomUAIDBhWPbSKQUjIAl
						lQEkkEwJLZauLkEC0Ug1hggCAvz4/maSKX+jgkxIGUQj5DskufbelsOJIeXlfNfF7D1VYi2FN7JL
						JAVQoiDGmgIYimT2NZMMiARhUY6ubILAS64AxDws2VrLzIVUy4PYCUQEJTFCSIkIUGD6NrMHI4Fs
						pJAtmqiQZc2CRsnPnkAAVjsBISJEpcgIhCCSwcXry1igXppt3SKlTlJKCiTFSEsrF0UHErBK1/JB
						EaWg/jglkJC6L5tSIAAkZElIWMXBoDIJUACa70EQ1VIrS14PDCkTANlibKhS4SdjqKfvph0PvYtS
						Iw7LzflyuZzPmSsilQr0JG1DoLJkC5iS9VwSWIrRkQSVArAgBEtRKtGWWNfU4bgql4hliYZA03Fl
						t/UJMMjAAoRaF5HMhrCVogJIEEqBJIkASfUQw6aCJAiVAfQ/s/4diaxXKFpOe++w6KmWjUiweYVT
						simLCApiEsxU7TxloQEgwDYMwbBxVRdC6ZuAmEB9kRBAJwkLsPwhUb586R/Yyh0AgMrwiFKQWbcL
						b2fY9QBE6hANQWXvWpfD1dKu135/f36VzBSgpJLRxiNH2j8FUjlXwy7Pstqsx0KHUgmg3+fpdFAC
						EWtqObRmT7mgJbKRmQmUX4IYXBhKMCT7K6oRKRJKsAFAMtAY0TsiQswgSQYIpGQp8JIFIPmZAyFm
						ZmtNgGzgbOa5kEytql1oEMIyBEERDAwDZV/WVTIyVidzKFxEQISkVDJoxU2xnDWUJENMCCLUgQgI
						jRR62oEwidaVDcOEQww2hM1wk1Bm2Q5XKTCafY2oQxxbWy790s+36yowWK6BUh8On7Yr03XZ+aUN
						B1tCmasEScfjQiqidagBSyxLZHYxoUYODwek0Gi/3cElyRYCMjPITLA50unBBUxGA4JBYB12T0QA
						BGjDRfqzKYWE1kK5LuSaHQpKgDcqvK8k0UCFt4dokqjhHb39JEllBhf7eg5fGWTXGhGZSRBMkI32
						kGTk3Ej7lkATuihFgEnHSRU89QpYCJAOcsBAznVPuxaqOezS8DdB5rDo9izZDveX27yc13W1128s
						T6FEjyBs/Ry4MICwdCCltJo1ZgQVgQSDXblguMkEvWpKAsAyHTXRgZaAILbWJDU2SWoL1IlhuahI
						wQbCiw0gmjIjQEGimCBB2KFKAlqE3SIIMKRE8wZwKeNAW0UbmzIbU3hLcICgYN2WAKVixGKkkMmI
						6P3S6HiVwkqyk475yk9j2HmSKZK26hiRXU8Mfap7kCCEaGsoWTMtKBIjoATFoO3RmnF1POGw3J/X
						tZ/XS4oICFJQ04YJfYmQsisAhZTItMeWlnaQ1HvGCKSWtghaIC3eRSK5AkgEM33HjVGGJrL3HlHy';
		}
		
		//$data = urldecode($data);
		list($typ, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);

		

		base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));


		$image = WorkUpImage::add($fileName,$fileExt);

		$userImage = new UserImage();
		if($type == 'couverture') {

			// Move file to storage
			file_put_contents($filePath  . $fileName . '.' . $fileExt, $data);
			$userIm = UserImage::where('user_id',$id)->where('active',true)->where('type','couverture')->first();
			if (!empty($userIm)) {
				$userIm->active = false;
				$userIm->save();
			}
			$userImage->type = 'couverture';
		}else {
		
			// Move file to storage
			file_put_contents($filePath . $fileName . '.' . $fileExt, $data);
			$userIm = UserImage::where('user_id',$id)->where('active',true)->where('type','profil')->first();
			if (!empty($userIm)) {
				$userIm->active = false;
				$userIm->save();
			}
			$userImage->type = 'profil';
		}

		$userImage->user_id = $id;
		$userImage->image_id = $image->id;
		if (!$userImage->save()) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();
		}
		$output = array();
		$output['type'] = $type;
		if($type == 'couverture') {
			$output['response'] = 'public/uploads/user/' . $id . '/cover-picture/' . $fileName . '.' . $fileExt;
		} 
		else {
			$output['response'] = 'public/uploads/user/' . $id . '/profil-picture/' . $fileName . '.' . $fileExt;
		}
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
		exit();
	}

	public function postPublicLogin() {
        // Check if the form validates with success
		if ( $this->checkLogin() ){
			// return var_dump($this->credentials);
			// redirect the user back to the intended page
			// or defaultpage if there isn't one
			if (Auth::attempt($this->credentials, true)) {
				//track user
				parent::track('loggin', 'Auth', Auth::user()->id);

				//return Redirect::intended('/');
				if (Request::ajax()) {
					return Response::json(array('statut' => 'success', 'message' => 'Vous êtes maintenant connecté !', 'user' => User::getIdByAuth(Auth::user())));
				} else {
					return Redirect::intended('/');
				}
			} else {
				$user = AuthUser::where('email', Input::get('email'))->first();

				if ( empty($user) || !isset($user) ) {
					if (Request::ajax()) {
						return Response::json(array('statut' => 'danger', 'message' => 'Email inconnu !'));
					} else {
						return Redirect::back()->with('error', 'Email inconnu !')->withInput(Input::except('password'));
					}
				}
				if (Request::ajax()) {
						return Response::json(array('statut' => 'danger', 'message' => 'Mot de passe incorrect !'));
					} else {
						return Redirect::back()->with('error', 'Mot de passe incorrect !')->withInput(Input::except('password'));
					}
			}
			$this->user = $user;
			return Redirect::to('/');
		}
		if (Request::ajax()) {
			return Response::json(array('statut' => 'danger', 'message' => 'Mot de passe incorrect !'));
		} else {
			return Redirect::back()->withInput()->withErrors($this->validator);
		}
	}


	public function checkLogin()
	{
		//Login the user
		$this->credentials = array(
			'email'    => Input::get( 'email' ),
			'password' => Input::get( 'password' ));

		// Validate the inputs
		$this->validator = Validator::make(Input::all(), Config::get('validator.login'));

        // Check if the form validates with success
		return $this->validator->passes();
	}


		
		//Functions for Mobility
	/**
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @param  [type]
	 * @return [type]
	 */
	public function mInscriptionClient($birthday,$mail,$last_name,$phone,$adress,$postal_code,$first_name,$password,$isMale,$city) {
		if (AuthUser::where('email', urldecode($mail))->first()) {
			$output = array();
			$output['response'] = 'exist';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();
		}
		$user = User::add(urldecode($isMale),urldecode($last_name),urldecode($first_name),urldecode($mail),urldecode($phone),urldecode($adress),urldecode($postal_code),urldecode($city),urldecode($birthday));
		if (!empty($user)) {
			Client::add($user);
			$auth = AuthUser::add(urldecode($mail), MCrypt::decrypt(urldecode($password)));
				if (!empty($user) && !empty($auth)) {
					User::find($user->id)->auths()->save($auth);
					$output = array();
					$output['response'] = 'true';
					$output['user_id'] = $user->id;
					$data = Response::json([$output])->header('Content-Type', 'application/json');
					echo $data->getContent();
					exit();
				}
		}
		$output = array();
		$output['response'] = 'false';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
	}

	public function mInscriptionAutoE($siren,$birthday,$mail,$last_name,$job_category,$phone,$adress,$postal_code,$first_name,$biography,$password,$isMale,$city) 
	{
		if (AuthUser::where('email', urldecode($mail))->first()) {
			$output = array();
			$output['response'] = 'exist';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();
		}
		if (!AutoContractor::where('siren',$siren)->first()) {
			$user = User::add(urldecode($isMale),urldecode($last_name),urldecode($first_name),urldecode($mail),urldecode($phone),urldecode($adress),urldecode($postal_code),urldecode($city),urldecode($birthday));
			if (!empty($user)) {
				$autoContractor = AutoContractor::add($user,urldecode($siren),urldecode($biography));
				if (is_numeric($job_category)) {
					$autoContractor->attachJobCategory($job_category);
				}
				$auth = AuthUser::add(urldecode($mail), MCrypt::decrypt(urldecode($password)));
					if (!empty($auth)) {
						User::find($user->id)->auths()->save($auth);
						$output = array();
						$output['response'] = 'true';
						$output['user_id'] = $user->id;
						$data = Response::json([$output])->header('Content-Type', 'application/json');
						echo $data->getContent();
						exit();
				}
			}
		}
		$output = array();
		$output['response'] = 'false';
		$output['error'] = 'SIREN exist';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
	}

	public function mConnexion($password,$mail) {
		$auth = AuthUser::where('email', urldecode($mail))->first();
		if (empty($auth)) {
			$output = array();
			$output['response'] = 'false';
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();
		}
		$credentials = array(
			'email'    => urldecode($mail),
			'password' => MCrypt::decrypt(urldecode($password)));
		if (Auth::attempt($credentials, true)) {
			$user = User::where('mail',urldecode($mail))->first();
			$output = array();
			$output['id'] = $user->id;
			$output['first_name'] = $user->first_name;
			$output['last_name'] = $user->last_name;
			$output['city'] = $user->city;
			$output['adress'] = $user->address;
			$output['mail'] = $user->mail;
			$output['phone'] = $user->phone;
			$output['postal_code'] = $user->postal_code;
			$output['isMale'] = $user->isMale;
			$output['birthday'] = $user->birthday;
			$output['couverture'] = $user->getPathPicture('couverture');
			$output['profil'] = $user->getPathPicture('profil');
			
			if (Client::where('user_id',$user->id)->first()) {
				$output['response'] = 'CU';
				$data = Response::json([$output])->header('Content-Type', 'application/json');
				echo $data->getContent();
				exit();
			}
			$ae = AutoContractor::where('user_id',$user->id)->first();
			$output['response'] = 'AE';
			$output['biography'] = $ae->biography;
			$output['siren'] = $ae->siren;
			$output['job_category'] = $ae->getJobCategory();
			$data = Response::json([$output])->header('Content-Type', 'application/json');
			echo $data->getContent();
			exit();
		}
		$output = array();
		$output['response'] = 'false';
		$data = Response::json([$output])->header('Content-Type', 'application/json');
		echo $data->getContent();
	}

	public function mForgotPassword($mail) {
		// Test if the email exist in BDD
			$user_mail = User::where('mail',urldecode($mail))->first();
			if (empty($user_mail)) {
				$output = array();
				$output['response'] = 'not exist';
				$data = Response::json([$output])->header('Content-Type', 'application/json');
				echo $data->getContent();
				exit();
			}
			$auth = AuthUser::where('email',urldecode($mail))->first();
			//envoi de mail
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPAuth   = true;
	        $mail->Host       = "smtp.gmail.com";
	        $mail->Port       = 587;
	        $mail->Username   = "Workup.Project@gmail.com";
	        $mail->Password   = "kLd2KMMx";
	        $mail->smtpConnect( array(
	        	"ssl" => array(
	            "verify_peer" => false,
	            "verify_peer_name" => false,
	            "allow_self_signed" => true
        		)
    		));
	        $mail->SMTPSecure = 'tls';                                    
			$mail->SetFrom('Workup.Project@gmail.com','Work-Up');
			$mail->FromName = 'Client Work-Up';
			$mail->addAddress($user_mail->mail);         
			$mail->isHTML(false);                                  
			$mail->Subject = 'Procedure de recuperation de mot de passe';
			$mail->Body    = "Votre mot de passe pour l'application Work-Up :'". $auth->password ."'";
			$mail->AltBody = 'Work-Up vous remercie pour votre intérêt ! :)';
			if(!$mail->send()) {
				$output = array();
				$output['response'] = 'false';
				$output['error'] = 'Erreur lors de l envoi du mail';
				$data = Response::json([$output])->header('Content-Type', 'application/json');
				echo $data->getContent();
			} else {
				$output = array();
				$output['response'] = 'true';
				$data = Response::json([$output])->header('Content-Type', 'application/json');
				echo $data->getContent();
			}
	}

	public function mGetAllCategory () {
		return JobCategory::all(['libelle','id'])->toJson();
	}
}