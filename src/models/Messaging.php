<?php
class Messaging extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'messaging';
    public $timestamps = true;
}