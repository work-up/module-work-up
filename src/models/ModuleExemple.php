<?php

class ModuleExemple extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'exemple';
    public $timestamps = true;

    
    //Navigable
    public static $presenter = 'template::admin.template.presenter';
    public static $rscName = 'template::admin.rscTemplate';

    /**
     * #Pager method
     *
     * @return mixed
     */
    public function renderResource()
    {
		//Your view return into a page
    }
}