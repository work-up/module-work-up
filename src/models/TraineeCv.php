<?php
class TraineeCv extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'trainee_cv';
    public $timestamps = true;
}