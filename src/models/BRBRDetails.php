<?php
class BRBRDetails extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'b_r_b_r_details';
    public $timestamps = true;
}