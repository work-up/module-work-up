<?php
class Budget extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'budget';
    public $timestamps = true;
}