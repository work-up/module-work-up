<?php
class BenefitRequest extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefit_request';
    public $timestamps = true;

    public static function generateBenefitNumber() {
    	$prefix = '130305';
    	$benefit = self::orderBy('id', 'DESC')->first();
    	if (!empty($benefit)) {
    		return $prefix . $benefit->id + 1;
    	}
    	return $prefix . '1';
    }

    public static function generateBenefitNumberForHuman() {
    	$numero = self::generateBenefitNumber();
    	return 'Numéro de la demande : ' . $numero;
    }

    public static function add($titre = null,$description = null, $dateDebutSouhaite = null, $budgetPrestation = null, $ville = null, $userId, $userPublicId) {
    	
    	$ae = AutoContractor::where('user_id', $userPublicId)->first();
    	$benefitRequest = new self();
    	$benefitRequest->user_id = $userId;
    	$benefitRequest->auto_contractor_id = $ae->id;
    	$benefitRequest->description = $description;
        $benefitRequest->debut_date = $dateDebutSouhaite;
    	$benefitRequest->budget = $budgetPrestation;
    	$benefitRequest->city = $ville;
    	$benefitRequest->title = $titre;
    	$benefitRequest->state = 'en cours';
    	if ($benefitRequest->save()) {
    		return true;
    	}
    	return false;
    }

    public function getRedactorName() {
        $benefit = self::find($this->id);
        $user = User::find($benefit->user_id);
        if(!empty($user)) {
            return $user->last_name . ' ' . $user->first_name;
        }
        return null;
    }

    public function getRedactorMail() {
        $benefit = self::find($this->id);
        $user = User::find($benefit->user_id);
        if(!empty($user)) {
            return $user->mail;
        }
        return null;
    }

     public function getDestinationName() {
        $benefit = self::find($this->id);
        $ae=AutoContractor::find($benefit->auto_contractor_id);
        $user = User::find($ae->user_id);
        if(!empty($user)) {
            return $user->last_name . ' ' . $user->first_name;
        }
        return null;
    }

    public function getDestinationMail() {
        $benefit = self::find($this->id);
        $ae=AutoContractor::find($benefit->auto_contractor_id);
        $user = User::find($ae->user_id);
        if(!empty($user)) {
            return $user->mail;
        }
        return null;
    }

    public function getRedactorPhone() {
        $benefit = self::find($this->id);
        $user = User::find($benefit->user_id);
        if(!empty($user)) {
            return $user->phone;
        }
        return null;
    }

    public function getRedactorCity() {
        $benefit = self::find($this->id);
        $user = User::find($benefit->user_id);
        if(!empty($user)) {
            return $user->city;
        }
        return null;
    }

    public function getRedactorAdress() {
        $benefit = self::find($this->id);
        $user = User::find($benefit->user_id);
        if(!empty($user)) {
            return $user->address . ', ' . $user->postal_code . ' ' . $user->city ; 
        }
        return null;
    }

    public function getDestinationAdress() {
        $benefit = self::find($this->id);
        $ae=AutoContractor::find($benefit->auto_contractor_id);
        $user = User::find($ae->user_id);
        if(!empty($user)) {
            return $user->address . ', ' . $user->postal_code . ' ' . $user->city ; 
        }
        return null;
    }

    public function getRedactorCompagny() {
        $benefit = self::find($this->id);
        $ae=AutoContractor::find($benefit->auto_contractor_id);
        if(!empty($ae)) {
            return $ae->compagny; 
        }
        return null;
    }


    public function getCreateDate() {

    }

    public function hasDetails() {
    if (BenefitRequestDetails::where('benefit_request_id',$this->id)->first()) {
      return true;
    }
    return false;
  }

  public function getDetailsMessage() {
    if ($this->hasDetails()) {
        $brd = BenefitRequestDetails::where('benefit_request_id',$this->id)->first();
        return $brd->message;
    }
    return null;
  }

  public function getDetailsResponse() {
    if ($this->hasDetails()) {
        $brd = BenefitRequestDetails::where('benefit_request_id',$this->id)->first();
        return $brd->response;
    }
    return null;
  }

  public function getQuotations($state) {
    $brqs = BenefitRequestQuotation::where('benefit_request_id', $this->id)->where('state',$state)->get();
    $tab = array();
    foreach ($brqs as $brq) {
        $quotation = Quotation::find($brq->id);
        $tab[] = $quotation;
    }
    return $tab;
  }

  public function getAllQuotations() {
    $brqs = BenefitRequestQuotation::where('benefit_request_id', $this->id)->get();
    $tab = array();
    foreach ($brqs as $brq) {
        $quotation = Quotation::find($brq->id);
        $tab[] = $quotation;
    }
    return $tab;
  }
}