<?php

class WorkUpGallery extends Gallery {


	public static function add($coverImage = null) {
		$gallery = new Gallery();
		$gallery->i18n_description = 1;
		$gallery->cover_image_id = $coverImage;
		if($gallery->save()) {
			return $gallery;
		}
		return null;
	}

	public function images() {
        return $this->belongsToMany('Gallery', 'gallery_image', 'gallery_id', 'image_id');
    }
}