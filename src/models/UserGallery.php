<?php
class UserGallery extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_gallery';

	public function gallery () {
		return $this->belongsTo('Gallery','gallery_id','id');
	}

	public static function add($id,$nom) {
		$ug = new self();
		$ug->user_id = $id;
		$gallery = WorkUpGallery::add();
		if (!$gallery->save()) {
			return null;
		}
		$ug->gallery_id = $gallery->id;
		$ug->name = $nom;
		if ($ug->save()) {
			return $ug;
		}
		return null;
	}

	public static function supp($id,$nom) {
		$ug = self::where('user_id',$id)->where('name',$nom)->first();
		if(empty($ug)) {
			return false;
		}
		$gallery = WorkUpGallery::where('id',$ug->gallery_id)->first();
		if(!$ug->delete()){
			return false;
		}
		if(!$gallery->delete()){
			return false;
		}
		return true;

	}


}