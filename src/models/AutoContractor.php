<?php
class AutoContractor extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'auto_contractor';
    public $timestamps = true;

    public static function add($user,$siren,$biography,$compagny) {
    	$auto = new self();
    	$auto->user_id = $user->id;
        $auto->siren = $siren;
        $auto->biography = $biography;
    	$auto->compagny = $compagny;
    	if ($auto->save()) {
    		return $auto;
    	}
    	return null;
    }


    public function attachJobCategory($jobCategory_id) {
        $exist = AutoContractorJobCategory::where('auto_contractor_id',$this->id)->first();
        if (!empty($exist)) {
            AutoContractorJobCategory::destroy($exist->id);
        }
        $auto = new AutoContractorJobCategory();
        $auto->job_category_id = $jobCategory_id;
        $auto->auto_contractor_id = $this->id;
        if ($auto->save()) {
            return true;
        }
        return false;
    }

    public function getJobCategory() {
        $cat = AutoContractorJobCategory::where('auto_contractor_id',$this->id)->first();
        if(!empty($cat)) {
            $jobCat = JobCategory::where('id',$cat->job_category_id)->first();
            if(!empty($jobCat)) {
                return $jobCat->libelle;
            }
        }
        return null;
    }

    public function getBenefitRequest($state) {
       $benefits = BenefitRequest::where('auto_contractor_id',$this->id)->where('state',$state)->get();
       if (!$benefits->isEmpty()) {
            return $benefits;
       }
       return null;
    }

    public function getNbBenefitRequest($state) {
        $benefits = BenefitRequest::where('auto_contractor_id',$this->id)->where('state',$state)->get();
        return count($benefits);
    }

    public function getQuotation($state) {
        $benefits = BenefitRequest::where('auto_contractor_id',$this->id)->where('state','accepte')->get();
        $arr = array();
        foreach($benefits as $benefit) {
            $results = $benefit->getQuotations($state);
            $arr2 = array();
            foreach ($results as $result) {
                $arr2[] = $result;
            }
            $arr[] = $arr2;
        }
        return $arr;
    }

    public function getAllQuotation() {
        $benefits = BenefitRequest::where('auto_contractor_id',$this->id)->get();
        $arr = array();
        foreach($benefits as $benefit) {
            $results = $benefit->getAllQuotations();
            $arr2 = array();
            foreach ($results as $result) {
                $arr2[] = $result;
            }
            $arr[] = $arr2;
        }
        return $arr;
    }
}