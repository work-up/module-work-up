<?php
class BenefitRequestQuotation extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefit_request_quotation';
    public $timestamps = true;
}