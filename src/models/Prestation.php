<?php
class Prestation extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'prestation';
    public $timestamps = true;
}