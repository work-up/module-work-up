<?php
class BenefitRequestDetails extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefit_request_details';
    public $timestamps = true;

    public static function addMessage($message,$idRequest) {
    	$brd = new self();
    	$brd->message = $message;
    	$brd->benefit_request_id = $idRequest;
    	if (!$brd->save()) {
    		return false;
    	}
    	return true;
    }

    public static function addResponse($response,$idRequest) {
    	$brd = self::where('benefit_request_id',$idRequest)->first();
    	$brd->response = $response;
    	if (!$brd->save()) {
    		return false;
    	}
    	return true;
    }
}