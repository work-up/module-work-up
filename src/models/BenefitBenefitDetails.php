<?php
class BenefitBenefitDetails extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefit_benefit_details';
    public $timestamps = true;
}