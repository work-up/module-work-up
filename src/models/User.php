<?php
class User extends Eloquent{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    public $timestamps = true;

    public function auths () {
        return $this->belongsToMany('AuthUser','auth_user','user_id','auth_id');
    }

    public static function getLoginForm () {
        return self::sanitize_output(View::make('theme::public.user.components.login-form')->render());
    }
    public static function getLoginMinForm () {
        return self::sanitize_output(View::make('theme::public.user.components.login-min-form')->render());
    }

     public static function sanitize_output ($buffer) {
        $search = array(
            '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
            '/[^\S ]+\</s',  // strip whitespaces before tags, except space
            '/(\s)+/s'       // shorten multiple whitespace sequences
        );

        $replace = array(
            '>',
            '<',
            '\\1'
        );

        $buffer = preg_replace($search, $replace, $buffer);

        return $buffer;
    }

    public static function getIdByAuth () {
        if (Auth::check()) {
            $auth = Auth::user();
            if (!empty($auth)) {

                $authUser = DB::table('auth_user')->where('auth_id', $auth->id)->first(array('user_id'));
                if (!empty($authUser)) {
                    return $authUser->user_id;
                }
                return 'user_not_found';
            }
        }
        return 'unlogged';
    }

    public static function add($isMale,$last_name,$first_name,$mail,$phone,$adress,$postal_code,$city,$birthday) {
        $user = new self();
        $user->isMale = $isMale;
        $user->last_name = $last_name;
        $user->first_name = $first_name;
        $user->mail = $mail;
        $user->phone = $phone;
        $user->address = $adress;
        $user->postal_code = $postal_code;
        $user->city = $city;
        $user->birthday = $birthday;
        $slug = strtolower($user->last_name) . '.' . strtolower($user->first_name);
        if(!User::where('slug',$slug)->first()) {
            $user->slug = $slug;
        } else {
            $user->slug = $slug . uniqid();
        }
        if ($user->save()) {
            return $user;
        }
        return null;
    }

    public function getName() {
        return strtoupper($this->last_name) . ' ' . ucfirst($this->first_name);
    }

    public function getMetier() {
        $ae = AutoContractor::where('user_id',$this->id)->first();
        if(!empty($ae)) {
            $ae_cat = AutoContractorJobCategory::where('auto_contractor_id',$ae->id)->first();
            if(!empty($ae_cat)) {
                $job_cat = JobCategory::where('id',$ae_cat->job_category_id)->first();
                return $job_cat->libelle;
            }
        }
        return null;
    }

    public function getCity(){
        return ucfirst($this->city);
    }

    public function isAuto() {
        if (AutoContractor::where('user_id',$this->id)->first()) {
            return true;
        }
        return false;
    }

    public function getDescription() {
         $ae = AutoContractor::where('user_id',$this->id)->first();
           if (!empty($ae) ) {
            return $ae->biography;
         }
         return 'L\'auto entrepreneur n\'a pas rempli sa description';
    }

    public function getCompagny() {
         $ae = AutoContractor::where('user_id',$this->id)->first();
           if (!empty($ae) ) {
            return $ae->compagny;
         }
         return 'L\'auto entrepreneur n\'a pas rempli sa description';
    }

    public function getPrivateDescription() {
         $ae = AutoContractor::where('user_id',$this->id)->first();
           if (!empty($ae) ) {
                if(!empty($ae->biography)) {
                    return $ae->biography;
                }
         }
         return 'Veuillez renseigner votre biographie, expérience...';
    }

    public function hasProfilPicture() {
        if (UserImage::where('user_id',$this->id)->where('type','profil')->first()) {
            return true;
        }
        return false;
    }

    public function hasCoverPicture() {
        if (UserImage::where('user_id',$this->id)->where('type','couverture')->first()) {
            return true;
        }
        return false;
    }

    public function hasGallery() {
        if (UserGallery::where('user_id',$this->id)->first()) {
            return true;
        }
        return false;
    }

    public function getPathPicture($type) {
        if ($type == 'profil') {
            if ($this->hasProfilPicture()) {
                $userImage = UserImage::where('user_id',$this->id)->where('type','profil')->where('active',1)->first();
                $image = Image::where('id',$userImage->image_id)->first();
                return  asset('/uploads/user/' . $this->id . '/profil-picture/' . $image->file_name . '.' . $image->file_ext);
            } else {
                return asset('uploads/system/default-picture/profile.jpg');
            }
        }
        if ($type == 'couverture') {
             if ($this->hasCoverPicture()) {
                $userImage = UserImage::where('user_id',$this->id)->where('type','couverture')->where('active',1)->first();
                $image = Image::where('id',$userImage->image_id)->first();
                return  asset('/uploads/user/' . $this->id . '/cover-picture/' . $image->file_name . '.' . $image->file_ext);
            } else {
                return asset('uploads/system/default-picture/cover.jpg');
            }
        }
    }

    public function getBenefitRequest($state) {
       $benefits = BenefitRequest::where('user_id',$this->id)->where('state',$state)->get();
       if (!$benefits->isEmpty()) {
            return $benefits;
       }
       return null;
    }

    public function getNbBenefitRequest($state) {
        $benefits = BenefitRequest::where('user_id',$this->id)->where('state',$state)->get();
        return count($benefits);
    }

    public function getQuotation($state) {
        $benefits = BenefitRequest::where('user_id',$this->id)->where('state','accepte')->get();
        $arr = array();
        foreach($benefits as $benefit) {
            $results = $benefit->getQuotations($state);
             $arr2 = array();
            foreach ($results as $result) {
                $arr2[] = $result;
            }
            $arr[] = $arr2;
        }
        return $arr;
    }

    public function getAllQuotation() {
        $benefits = BenefitRequest::where('user_id',$this->id)->get();
        $arr = array();
        foreach($benefits as $benefit) {
            $results = $benefit->getAllQuotations();
            $arr2 = array();
            foreach ($results as $result) {
                $arr2[] = $result;
            }
            $arr[] = $arr2;
        }
        return $arr;
    }
}