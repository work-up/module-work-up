<?php
class Client extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'client';
    public $timestamps = true;


    public static function add ($user) {
    	//Slug + userID
    	$client = new self();
    	$client->user_id = $user->id;
    	if ($client->save()) {
    		return true;
    	}
    	return false;
    }
}