<?php
class Quotation extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'quotation';
    public $timestamps = true;


     public static function add($br) {
    	$quot = new self();
    	if ($quot->save()) {
    		$brq = new BenefitRequestQuotation();
	    	$brq->benefit_request_id = $br->id;
	    	$brq->quotation_id = $quot->id;
	    	$brq->state = 'a faire';
	    	if ($brq->save()) {
    			return true;
    		}
    	}
    	return false;
    }

    public function getBenefitInfos() {
    	$brq = BenefitRequestQuotation::where('quotation_id',$this->id)->first();
    	if(!empty($brq)) {
    		$br = BenefitRequest::find($brq->benefit_request_id);
    		return $br;
    	}
    	return null;
    }
}