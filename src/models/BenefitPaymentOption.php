<?php
class BenefitPaymentOption extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefit_payment_option';
    public $timestamps = true;
}