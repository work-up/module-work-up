<?php
class CategoryContractor extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'category_notation';
    public $timestamps = true;
}