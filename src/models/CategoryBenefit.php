<?php
class CategoryBenefit extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'category_benefit';
    public $timestamps = true;
}