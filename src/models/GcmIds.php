<?php
class GcmIds extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'gcm_ids';
    public $timestamps = true;
    private $url = 'https://gcm-http.googleapis.com/gcm/send';
    private $serverApiKey='AIzaSyB4E8x-z32g6CeUkiSsJtkzlBQDxVj-SS4';
    private $devices = array();

    public static function add($gcm_token,$userId) {
    	if (!self::where('user_id',$userId)->first()) {
	        $gcm = new self();
	        $gcm->gcm_token = $gcm_token;
	        $gcm->user_id = $userId;
	        if ($gcm->save()){
	            return true;
	        }
        }
        return false;
    }


 
    /**
     * Pour obtenir l'Utilisateur
     */
    public function getDevices($userId) {
        $result = $this->mysqli->query("SELECT gcm_token FROM Gcm_ids WHERE user_id='".$userId."'");
        $gcm = GcmIds::where('user_id',$userId)->first();
        return $gcm;
    }

    public function setDevices($deviceIds){
 
        if(is_array($deviceIds)){
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }
 
    }
 
    /*
        Send the message to the device
        @param $message The message to send
        @param $data Array of data to accompany the message
    */
    public static function send($message, $userId, $data = false, $collapse_key = null, $time_to_live = null){
 		
 		$devices = (array) self::select('gcm_token')->where('user_id',$userId)->first();


 		if(empty($devices)) {
 			return false;
 		}
 		$url = 'https://gcm-http.googleapis.com/gcm/send';
    	$serverApiKey='AIzaSyB4E8x-z32g6CeUkiSsJtkzlBQDxVj-SS4';

        if(!is_array($devices) || count($devices) == 0){
            return false;
        }
 
        if(strlen($serverApiKey) < 8){ 
            return false;;
        }
 
        $fields = array(
            'registration_ids'  => $devices,
            'data'              => array( "message" => $message ),
        );
 
        if ($time_to_live != null) {
            $fields['time_to_live'] = $time_to_live;
        }
 
        if ($collapse_key != null) {
            //$fields['delay_while_idle'] = true;
            $fields['collapse_key'] = $collapse_key;
        }
 
       if(is_array($data)){
            foreach ($data as $key => $value) {
                $fields['data'][$key] = $value;
            }
        }
        
 
        $headers = array(
            'Authorization: key=' . $serverApiKey,
            'Content-Type: application/json'
        );

 
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
 
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
 
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
 
        // Avoids problem with https certificate
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
 
        // Execute post
        $result = curl_exec($ch);
 
        // Close connection
        curl_close($ch);
 
        return true;
    }
 
    public function error($msg){
        echo "Android send notification failed with error:";
        echo "\t" . $msg;
        exit(1);
    }
}