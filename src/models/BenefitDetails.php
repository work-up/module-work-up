<?php
class BenefitDetails extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefit_details';
    public $timestamps = true;
}