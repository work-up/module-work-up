<?php
class AutoContractorJobCategory extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'auto_contractor_job_category';
    public $timestamps = true;
}