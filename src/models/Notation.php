<?php
class Notation extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'notation';
    public $timestamps = true;
}