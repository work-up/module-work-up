<?php
class PaymentOption extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'payment_option';
    public $timestamps = true;
}