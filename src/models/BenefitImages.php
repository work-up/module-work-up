<?php
class BenefitImages extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefit_images';
    public $timestamps = true;
}