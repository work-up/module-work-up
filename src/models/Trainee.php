<?php
class Trainee extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'trainee';
    public $timestamps = true;
}