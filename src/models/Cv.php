<?php
class Cv extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cv';
    public $timestamps = true;
}