<?php

class WorkUpImage extends Image {


	public static function add($fileName,$fileExt) {
		$image = new Image();
		$image->i18n_description = null;
		$image->file_name = $fileName;
		$image->file_ext = $fileExt;
		if($image->save()) {
			return $image;
		}
		return null;
	}

}