<?php
class StateStatus extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'state_status';
    public $timestamps = true;
}