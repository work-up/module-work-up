<?php
class JobCategory extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'job_category';
    public $timestamps = true;


    public static function allCached(){
		return Cache::remember('job_category', 10080, function() {
			$result = JobCategory::all();
			return $result;
		});
    }

	public static function getOptions ($input = null) {
		$data = self::allCached();
		return View::make('theme::public.user.subviews.optionJobCategory', array('result' => $data, 'input' => $input))->render();
	}

	public static function add($libelle) {
		$jc = new self();
		$jc->libelle  = $libelle;
		if (!$jc->save()){
			return false;
		}
		return true;
	}
}