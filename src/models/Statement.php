<?php
class Statement extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'statement';
    public $timestamps = true;
}