<?php 

namespace Dynamix\WorkUp;

use Illuminate\Support\ServiceProvider;

class WorkUpServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;
	
	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('dynamix/workup');

		// Get config loader
		$loader = $this->app['config']->getLoader();
		// Get environment name
		$env = $this->app['config']->getEnvironment();
		// Add package namespace with path set base on your requirement
		$loader->addNamespace('core',__DIR__.'/../../config');

		// Load package override config file
		$configs = $loader->load($env,'route','core');
		// Override value
		$this->app['config']->set('core::route',$configs);

		$this->app->register('Barryvdh\DomPDF\ServiceProvider');
		$loader = \Illuminate\Foundation\AliasLoader::getInstance();
		$loader->alias('PDF','Barryvdh\DomPDF\Facade');

		// Include routes file if we need
		include_once __DIR__ . '/../../routes.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	    $this->app['workup'] = $this->app->share(function($app)
	    {
			return new WorkUp;
	    });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
