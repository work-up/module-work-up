<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitRequestDetailsTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('benefit_request_details', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();

			$table->integer('benefit_request_id')->unsigned();
	 		//$table->foreign('benefit_request_id')->references('id')->on('benefit_request');
			
			$table->string('message',1500)->nullable();
			$table->string('response',1500)->nullable();

			$table->nullableTimestamps();
		});

	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('benefit_request_details');
	}

}



