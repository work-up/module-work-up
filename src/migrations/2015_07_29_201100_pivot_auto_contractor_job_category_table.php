<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotAutoContractorJobCategoryTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('auto_contractor_job_category', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->integer('job_category_id')->unsigned();
			$table->foreign('job_category_id')->references('id')->on('job_category');
			$table->integer('auto_contractor_id')->unsigned();
			$table->foreign('auto_contractor_id')->references('id')->on('auto_contractor');
			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `client` table
		Schema::drop('auto_contractor_job_category');
	}

}