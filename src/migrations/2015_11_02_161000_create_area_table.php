<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('area', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->string('area_number',8);
			$table->string('city',25);
			$table->string('description',1500);	
			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('area');
	}

}



