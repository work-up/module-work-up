<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotBenefitPaymentOptionTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `user_gallery` table
 	Schema::create('benefit_payment_option', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();

		$table->integer('benefit_id')->unsigned();
		$table->foreign('benefit_id')->references('id')->on('benefit');

	 	$table->integer('payment_option_id')->unsigned();
	 	$table->foreign('payment_option_id')->references('id')->on('payment_option');


	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `user_gallery` table
		Schema::drop('benefit_payment_option');
	}
}