<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('cv', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->string('file_name',150);
			$table->longText('file_content');
			

			$table->string('remember_token')->nullable();
			$table->dateTime('last_visit_at');
			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('cv');
	}

}



