<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('benefit', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->integer('user_id')->unsigned();
			$table->string('title',50);
			$table->string('description',1500);
			$table->enum('state', ['en attente', 'refuse','termine', 'en cours']);
			$table->string('area_id');
			$table->dateTime('begin_date')->default(null);
			$table->dateTime('end_date')->default(null);
			$table->integer('auto_contractor_id')->unsigned();
			$table->integer('progress')->default(null);
			//$table->foreign('auto_contractor_id')->references('id')->on('auto_contractor');

			$table->string('remember_token')->nullable();
			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('benefit');
	}

}