<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('notifications', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->string('title',50)->nullable()->default(null);
			$table->string('description',250)->nullable()->default(null);

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');			

			$table->string('remember_token')->nullable();
			$table->dateTime('last_visit_at');
			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('notifications');
	}

}



