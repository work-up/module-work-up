<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotAuthUserTable extends Migration {

	/**
	 * Informations des personnes, active permet de savoir si le auth est actif
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `Comments` table
 	Schema::create('auth_user', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();
		$table->integer('user_id')->unsigned();
		$table->foreign('user_id')->references('id')->on('users');

	 	$table->integer('auth_id')->unsigned();
		$table->foreign('auth_id')->references('id')->on('auths');
	 	
	 	$table->boolean('active')->unsigned()->default(true);

	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `auth_user` table
		Schema::drop('auth_user');
	}
}