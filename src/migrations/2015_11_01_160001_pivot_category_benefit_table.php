<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotCategoryBenefitTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `Comments` table
 	Schema::create('category_benefit', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();
		$table->integer('category_id')->unsigned();
		$table->foreign('category_id')->references('id')->on('category');

	 	$table->integer('benefit_id')->unsigned();
	 	$table->foreign('benefit_id')->references('id')->on('benefit');
	 	
	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `imageuser` table
		Schema::drop('category_benefit');
	}
}