<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotUserImageTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `Comments` table
 	Schema::create('user_image', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();
		$table->integer('user_id')->unsigned();
		$table->foreign('user_id')->references('id')->on('users');

	 	$table->integer('image_id')->unsigned();
	 	$table->foreign('image_id')->references('id')->on('images');
	 	
	 	$table->enum('type',['profil','couverture']);;

	 	$table->boolean('active')->default(true);

	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `imageuser` table
		Schema::drop('user_image');
	}
}