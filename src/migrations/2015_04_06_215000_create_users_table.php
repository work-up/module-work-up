<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Informations des personnes
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('users', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->boolean('isMale')->nullable()->default(null);
			$table->string('last_name',50)->nullable()->default(null);
			$table->string('first_name',50)->nullable()->default(null);
			$table->string('mail',50)->nullable()->default(null);
			$table->string('phone',20)->nullable()->default(null);
			$table->dateTime('birthday',10)->nullable()->default(null);
			$table->string('address',250)->nullable()->default(null);
			$table->string('postal_code',10)->nullable()->default(null);
			$table->string('city',50)->nullable()->default(null);
			$table->double('image')->nullable()->default(null);			
			$table->string('region',50)->nullable()->default(null);

			$table->string('remember_token')->nullable()->default(null);

			//Add SLUG
			$table->string('slug')->nullable();

			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('users');
	}

}