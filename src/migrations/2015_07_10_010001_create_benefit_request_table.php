<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitRequestTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('benefit_request', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('auto_contractor_id')->unsigned();
			$table->foreign('auto_contractor_id')->references('id')->on('auto_contractor');

			$table->string('title',50)->nullable();
			$table->string('description',1500)->nullable();
			$table->dateTime('debut_date')->nullable();
			$table->enum('state', ['accepte', 'refuse', 'en cours', 'negociation'])->nullable();
			$table->string('budget',8)->nullable();
			$table->string('city')->nullable();


			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('benefit_request');
	}

}



