<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotBenefitBenefitDetailsTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `user_gallery` table
 	Schema::create('benefit_benefit_details', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();

		$table->integer('benefit_details_id')->unsigned();
		$table->foreign('benefit_details_id')->references('id')->on('benefit_details');

	 	$table->integer('benefit_id')->unsigned();
	 	$table->foreign('benefit_id')->references('id')->on('benefit');

	 	$table->integer('benefit_state')->unsigned();

	 	$table->integer('order');


	 	$table->boolean('active');

	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `user_gallery` table
		Schema::drop('benefit_benefit_details');
	}
}