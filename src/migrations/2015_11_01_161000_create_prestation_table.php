<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestationTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('prestation', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->string('wording',300);
			$table->string('url',300);
			$table->enum('state', ['Accepté', 'Refusé']);
			$table->integer('benefit_id')->unsigned();
			$table->foreign('benefit_id')->references('id')->on('benefit');
			

			$table->string('remember_token')->nullable();
			$table->dateTime('last_visit_at');

			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('prestation');
	}

}



