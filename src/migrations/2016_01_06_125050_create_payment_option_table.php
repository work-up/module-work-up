<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentOptionTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('payment_option', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->string('libelle',25);
			$table->float('price');
			$table->string('description',1500);
			$table->dateTime('reprieve');
			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('payment_option');
	}

}



