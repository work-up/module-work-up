<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagingTable extends Migration {

	/**
	 * Informations des personnes, withdrawn permet de savoir si la personne est active
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('messaging', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			
			$table->integer('source_id')->unsigned();
			$table->integer('destination_id')->unsigned();
			$table->string('object',25);
			$table->string('message',2000);
			$table->dateTime('date')->defaut(null);
			$table->integer('benefit_id')->unsigned();
			$table->foreign('benefit_id')->references('id')->on('benefit');
			

			$table->string('remember_token')->nullable();
			$table->dateTime('last_visit_at');

			
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `users` table
		Schema::drop('messaging');
	}

}



