<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotTraineeCvTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `user_gallery` table
 	Schema::create('trainee_cv', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();

		$table->integer('trainee_id')->unsigned();
		$table->foreign('trainee_id')->references('id')->on('trainee');

	 	$table->integer('cv_id')->unsigned();
	 	$table->foreign('cv_id')->references('id')->on('cv');

	 	$table->boolean('active');

	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `user_gallery` table
		Schema::drop('trainee_cv');
	}
}