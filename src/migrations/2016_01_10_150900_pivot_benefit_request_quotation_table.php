<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotBenefitRequestQuotationTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `user_gallery` table
 	Schema::create('benefit_request_quotation', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();

	 	$table->integer('benefit_request_id')->unsigned();
	 	$table->foreign('benefit_request_id')->references('id')->on('benefit_request');

		$table->integer('quotation_id')->unsigned();
		$table->foreign('quotation_id')->references('id')->on('quotation');

		$table->enum('state', ['accepte', 'refuse','en cours', 'a faire']);

	 	//$table->integer('benefit_state')->unsigned();
	 	//$table->foreign('benefit_state')->references('state')->on('benefit');

	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `user_gallery` table
		Schema::drop('benefit_request_quotation');
	}
}