<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotCategoryNotationTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `Comments` table
 	Schema::create('category_notation', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();
		$table->integer('notation_id')->unsigned();
		$table->foreign('notation_id')->references('id')->on('notation');

	 	$table->integer('category_id')->unsigned();
	 	$table->foreign('category_id')->references('id')->on('category');
	 	
	 	$table->integer('mark')->unsigned();
	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `imageuser` table
		Schema::drop('category_notation');
	}
}