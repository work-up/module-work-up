<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotUserGalleryTable extends Migration {

	/**
	 * Photo de profil, de couverture
	 *
	 * @return void
	 */
	public function up()
 {
 // Create the `user_gallery` table
 	Schema::create('user_gallery', function(Blueprint $table)
 	{
	  	$table->engine = 'InnoDB';
	 	$table->increments('id')->unsigned();

		$table->integer('user_id')->unsigned();
		$table->foreign('user_id')->references('id')->on('users');

	 	$table->integer('gallery_id')->unsigned();
	 	$table->foreign('gallery_id')->references('id')->on('galleries');

	 	$table->string('name');

	 	$table->nullableTimestamps();
 	});
 }
 /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `user_gallery` table
		Schema::drop('user_gallery');
	}
}