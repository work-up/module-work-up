<?php

//Route for i18n support
$locale = Localizr::initLocale();
Route::group(array('prefix' => $locale), function() 
{
	// Your route here !
});

//Route width /admin protection
// To use something here, see resource table
Route::group( array('before' => 'auth.admin', 'prefix' => 'admin') , function (){
	// Your admin route here
});
//homepage
Route::get('/', array('as' => 'homepage', 'uses' => 'ModuleWorkUpController@getHome'));


//Route with API style please
Route::get('/sign-up', array('as' => 'sign-up', 'uses' => 'ModuleWorkUpRegisterController@create'));
Route::post('/sign-up', array('as' => 'sign-up', 'uses' => 'ModuleWorkUpRegisterController@add'));

//---------------------Modal---------------------------------------------------------------
Route::post('/ajouter-photo-de-profil/', array('as' => 'photo-profil', 'uses' => 'ModuleWorkUpRegisterController@postImages'));
Route::post('/ajouter-photo-de-couverture', array('as' => 'photo-couverture', 'uses' => 'ModuleWorkUpRegisterController@postCoverImage'));
Route::post('/changement-mot-de-passe/', array('as' => 'changement-mot-de-passe', 'uses' => 'ProfilController@changePassword'));
Route::post('/ask-benefit-request/', array('as' => 'ask-benefit-request', 'uses' => 'BenefitController@postAskBenefitRequest'));
Route::post('/more-informations/', array('as' => 'more-informations', 'uses' => 'BenefitController@postAskMoreInformations'));
Route::post('/more-informations-response/', array('as' => 'more-informations-response', 'uses' => 'BenefitController@postResponseMoreInformations'));
Route::post('/create-devis/', array('as' => 'create-devis', 'uses' => 'PdfController@generateQuotation'));
Route::post('/dl-devis/', array('as' => 'dl-devis', 'uses' => 'PdfController@dlQuotation'));
Route::post('/post-contact/', array('as' => 'post-contact', 'uses' => 'ProfilController@postContact'));
//--------------------------------------------------------------------------------------------------------


Route::get('/forgot-password', array('as' => 'forgot-password', 'uses' => 'ModuleWorkUpRegisterController@createPassword'));
Route::post('/forgot-password', array('as' => 'forgot-password', 'uses' => 'ModuleWorkUpRegisterController@forgotPassword'));

Route::get('/sign-up-ae', array('as' => 'sign-up-ae', 'uses' => 'ModuleWorkUpRegisterController@createAe'));
Route::post('/sign-up-ae', array('as' => 'sign-up-ae', 'uses' => 'ModuleWorkUpRegisterController@add'));

Route::get('/search-result', array('as' => 'search-result', 'uses' => 'SearchController@createSearch'));
Route::post('/search-result', array('as' => 'search-result', 'uses' => 'SearchController@createSearch'));



Route::post('/login', array('as' => 'public.login.workup', 'uses' => 'ModuleWorkUpController@postPublicLogin'));

//Route for profil and edit
Route::get('/profil/{slug}', array('as' => 'profil-page', 'uses' => 'ProfilController@getPage'));
Route::post('/profil/{slug}', array('as' => 'profil-page-change', 'uses' => 'ProfilController@updateInfo'));
Route::get('/profil-page-edit', array('as' => 'profil-page-edit', 'uses' => 'ProfilController@createEdit'));
Route::post('/profil-page-edit', array('as' => 'profil-page-edit', 'uses' => 'ProfilController@add'));


//Module 3
Route::post('/ask-benefit', array('as' => 'ask-benefit', 'uses' => 'BenefitController@postAskBenefit'));
Route::get('/profil/{slug}/prestation-request', array('as' => 'global-benefit', 'uses' => 'BenefitController@getBenefitPage'));
Route::get('/profil/{slug}/prestation', array('as' => 'prestation', 'uses' => 'BenefitController@getPrestationPage'));
Route::get('/profil/{slug}/devis', array('as' => 'devis', 'uses' => 'BenefitController@getDevisPage'));


//***************Route for Mobile***********************
//Module 1
Route::get('/getSignUpCustomer/{birthday}/{mail}/{last_name}/{phone}/{address}/{postal_code}/{first_name}/{password}/{isMale}/{city}', array ('as' => 'inscription.client', 'uses' => 'ModuleWorkUpRegisterController@mInscriptionClient' ));
Route::get('/getSignUpAutoContractor/{siren}/{birthday}/{mail}/{last_name}/{job_category}/{phone}/{address}/{postal_code}/{first_name}/{biography}/{password}/{isMale}/{city}', array ('as' => 'inscription.autoe', 'uses' => 'ModuleWorkUpRegisterController@mInscriptionAutoE' ));
Route::get('/getSignIn/{password}/{mail}', array ('as' => 'connexion.mobility', 'uses' => 'ModuleWorkUpRegisterController@mConnexion' ));
Route::get('/getForgotPassword/{mail}', array ('as' => 'connexion.forgot', 'uses' => 'ModuleWorkUpRegisterController@mForgotPassword' ));
Route::get('/getAllCategoryType', array ('as' => 'connexion.allcategory', 'uses' => 'ModuleWorkUpRegisterController@mGetAllCategory' ));
Route::post('/addPicture', array ('as' => 'add.photo.profil', 'uses' => 'ModuleWorkUpRegisterController@mPostImage' ));
Route::get('/addPicture', array ('as' => 'add.photo.profil', 'uses' => 'ModuleWorkUpRegisterController@mPostImage' ));

//Module 2
Route::get('/updateInfosPersonnalClient/{id}/{birthday}/{adresse}/{codePostal}/{ville}/{telephone}', array ('as' => 'infos.update', 'uses' => 'ProfilController@mUpdateInfo' ));
Route::get('/updateInfosPersonnalAe/{id}/{adresse}/{birthday}/{codePostal}/{ville}/{telephone}/{experience}/{categoryJob}', array ('as' => 'infos.updateAe', 'uses' => 'ProfilController@mUpdateInfoAe' ));
Route::get('/getAlbum/{id}', array ('as' => 'get.album', 'uses' => 'ProfilController@mGetAlbum' ));
Route::get('/getGallery/{id}/{nomAlbum}', array ('as' => 'get.gallery', 'uses' => 'ProfilController@mGetGallery' ));
Route::get('/createAlbum/{id}/{nomAlbum}', array ('as' => 'create.album', 'uses' => 'ProfilController@mCreateAlbum' ));
Route::get('/deleteAlbum/{id}/{nomAlbum}', array ('as' => 'delete.album', 'uses' => 'ProfilController@mDeleteAlbum' ));
Route::get('/getPicture/{id}/{type}', array ('as' => 'get.picture', 'uses' => 'ProfilController@mGetPicture' ));
Route::post('/addPictureToAlbum', array ('as' => 'add.picture', 'uses' => 'ProfilController@mAddPicture' ));
Route::get('/addPictureToAlbum', array ('as' => 'add.picture', 'uses' => 'ProfilController@mAddPicture' ));
Route::get('/deletePictureToAlbum/{id}/{nomAlbum}/{fileName}', array ('as' => 'delete.picture', 'uses' => 'ProfilController@mDeletePicture' ));

//Module 3
Route::post('/createDemande', array ('as' => 'create.demande', 'uses' => 'ProfilController@mCreateAsk' ));
Route::get('/createDemande', array ('as' => 'create.demande', 'uses' => 'ProfilController@mCreateAsk' ));
Route::post('/contactMail', array ('as' => 'contact.mail', 'uses' => 'ProfilController@mContactMail' ));
Route::get('/contactMail', array ('as' => 'contact.mail', 'uses' => 'ProfilController@mContactMail' ));
Route::post('/responseDemande', array ('as' => 'response.demande', 'uses' => 'ProfilController@mResponseDemande' ));
Route::get('/responseDemande', array ('as' => 'response.demande', 'uses' => 'ProfilController@mResponseDemande' ));
Route::get('/searchProfil', array ('as' => 'search.profil', 'uses' => 'SearchController@mSearchProfil' ));
Route::post('/searchProfil', array ('as' => 'search.profil', 'uses' => 'SearchController@mSearchProfil' ));
Route::post('/getAsks', array ('as' => 'get.ask', 'uses' => 'ProfilController@mGetAsks' ));
Route::get('/getAsks', array ('as' => 'get.ask', 'uses' => 'ProfilController@mGetAsks' ));
Route::post('/getDevis', array ('as' => 'get.devis', 'uses' => 'ProfilController@mGetDevis' ));
Route::get('/getDevis', array ('as' => 'get.devis', 'uses' => 'ProfilController@mGetDevis' ));

Route::post('/registerGcm', array ('as' => 'register.gcm', 'uses' => 'NotifController@register' ));
Route::get('/registerGcm/{gcm_token}/{idUser}', array ('as' => 'register.gcm', 'uses' => 'NotifController@register' ));
Route::post('/getNotifications', array ('as' => 'get.notifications', 'uses' => 'NotifController@mGetNotifications' ));
Route::get('/getNotifications', array ('as' => 'get.notifications', 'uses' => 'NotifController@mGetNotifications' ));
Route::post('/responseAskDetails', array ('as' => 'get.responseAskDetails', 'uses' => 'ProfilController@mresponseAskDetails' ));
Route::get('/responseAskDetails', array ('as' => 'get.responseAskDetails', 'uses' => 'ProfilController@mresponseAskDetails' ));
Route::post('/createDevis', array ('as' => 'get.createDevis', 'uses' => 'ProfilController@mcreateDevis' ));
Route::get('/createDevis', array ('as' => 'get.createDevis', 'uses' => 'ProfilController@mcreateDevis' ));
Route::post('/updateDevis', array ('as' => 'get.updateDevis', 'uses' => 'ProfilController@mupdateDevis' ));
Route::get('/updateDevis', array ('as' => 'get.updateDevis', 'uses' => 'ProfilController@mupdateDevis' ));
Route::post('/getBenefits/{id}', array ('as' => 'get.getBenefits', 'uses' => 'ProfilController@mgetBenefits' ));
Route::get('/getBenefits/{id}', array ('as' => 'get.getBenefits', 'uses' => 'ProfilController@mgetBenefits' ));





Route::get('/pdf', array ('as' => 'pdf', 'uses' => 'PdfController@index' ));



