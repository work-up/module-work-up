<?php
/**
 * Validator
 *
 * all rules for forms in work up module
 */
return array(
	'sign-up' => array(
		'nom'			=> 'required',
		'prenom'		=> 'required',
		'postal_code'	=> 'required',
		'city'			=> 'required',
		'email'			=> 'required|email',
		'phone'			=> 'required|min:10|max:10',
		'password'		=> 'required|min:6',
		'vpassword'		=> 'required|min:6',
		'postal_code'   => 'min:5|max:5',
		),
	'ask-benefit' => array(
		'description' 			=> 'required',
		'dateDebutSouhaite' 	=> 'required',
		'budgetPrestation' 		=> 'required',
		),
);