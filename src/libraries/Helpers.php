<?php

use Illuminate\Support\Str;

class Helpers
{
	 public static function makeFileName($length = 8) {
        if ($length == 'rand') {
            $length = rand(8, 16);
        }
        return substr(md5(uniqid()), 0, $length);
    }

}