<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'ModuleExemple' => $baseDir . '/src/models/ModuleExemple.php',
    'ModuleTemplateDatabaseSeeder' => $baseDir . '/src/seeds/ModuleTemplateDatabaseSeeder.php',
    'ModuleWorkUpController' => $baseDir . '/src/controllers/ModuleWorkUpController.php',
);
