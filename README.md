# Module template
- [Presentation](#presentation)
- [What we can do](#what-we-can-do)
- [Architecture](#architecture)
- [Configuration](#configuration)
- [How to use](#how-to-use)

test

## <a name="presentation"></a>Presentation
Dynamix is composed with some modules for complete the core. The modules are extremely customisable for add features to the Dynamix core.

A module is placed in the vendor directory, example: /vendor/dynamix/template


## <a name="what-we-can-do"></a>What we can do
With a Dynamix module we can create all the same things than we can find in the Laravel framework, so :

- Routes
- Controllers
- Views
- Models
- Config vars
- Language files
- Migrations & Seeds
- Our own class and methods (in the Dynamix directory located in the src folder at root)
- And a public folder for Front-end features


## <a name="architecture"></a>Architecture
A module directory is composed with a public folder and a src folder. At the root we can find a composer.json for configure our autoloading. Please respect at maximum the PSR-0 (read more on PSR website about PHP conventions).

Here you can see an architecture example :

![](doc/architecture.jpg)


## <a name="configuration"></a>Configuration

When you create a module you should indicate the modules required for this one in the ServiceProvider.

Here is an example :
```php
    /**
	 * Module needed for this module
	 * @var array
	 */
	private $modules = array(
		'Gallery'
	);
```

You should add in the private $modules var your modules needed in the ServiceProvider. In a module you should obligatory keep the DependencyManager method in the boot method of the ServiceProvider ("\DependencyManager::init($this->modules);").


## <a name="how-to-use"></a>How to use

- [Routes](#routes)
- [Controllers](#controllers)
- [Database](#database)
- [Views](#views)
- [Config vars](#config-vars)
- [Language vars](#language-vars)
- [JS & SCSS & assets](#front-end)

### <a name="routes"></a>Routes

For create some routes it's easy, you should create a routes file (routes.php) in the src folder if this one doesnt exist. You should use this file like a normal route file of Laravel. (See the route file example in the src folder of the Module template).

Important: If you are using routes in your module your should add the path in the boot method in the service provider.

Example : 

```php
	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('dynamix/template');
		// Include routes file if we need
		include_once __DIR__ . '/../../routes.php';
	}
```

### <a name="controllers"></a>Controllers

For use controllers we should configure it in the composer.json file's auto-load section :

```json
    "autoload": {
        "classmap": [
            "src/migrations",
            "src/controllers"
        ],
        ......
    },
```

NB: Add it only if we use controllers in our module

### <a name="database"></a>Database

Migrations & seeding,

migrations, to migrate your database, make a composer install/update in your repo

when it done you can make this command :
```
php artisan migrate --bench="dynamix/template"
```

to seed, you must make a composer dump-autoload to create an autoload class map of your project
in the racine of dynamix make this command :
```
php artisan db:seed --class=ModuleTemplateDatabaseSeeder
```

that's all !


### <a name="views"></a>Views

We can use our own views for the module. For that we should add them in the views folder.
For call a view in our controllers we should simply do it :

```php
	View::make('myModule::foo.bar');
```

### <a name="config-vars"></a>Config vars

We can use our own config vars for the module. For that we should add them in the config folder.
For call a config var in our controllers we should simply do it :

```php
	Config::get('myModule::foo');
```


### <a name="language-vars"></a>Language vars

We can use our own language vars for the module. For that we should add them in the language folder.
For call a language var in our controllers we should simply do it :

```php
	Language::get('myModule::foo');
```

### <a name="front-end"></a>JS & SCSS & assets

For use the JS & SCSS & assets we should use GruntJS. It will automaticaly move the public files needed in the public root directory.
